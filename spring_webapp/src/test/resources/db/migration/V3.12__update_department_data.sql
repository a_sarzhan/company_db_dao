UPDATE department AS d
SET
    manager_id = c.manager_id
FROM (VALUES
    ('KZ_DEP_DEV_APP_0001', 10000),
    ('KZ_DEP_DEV_APP_0002', 10001),
    ('KZ_DEP_DEV_APP_0003', 10002),
    ('KZ_DEP_DEVOP_PRJ_0001', 10003),
    ('KZ_DEP_DEVOP_PRJ_0002', 10004),
    ('RU_DEP_PRJ_0001', 10010),
    ('RU_DEP_PRJ_0002', 10011),
    ('US_DEP_PRJ_0001', 10016),
    ('US_DEP_PRJ_0002', 10017),
    ('CA_DEP_PRJ_TEST_0001', 10021),
    ('CA_DEP_PRJ_TEST_0002', 10022)
) AS c(department_name, manager_id)
WHERE c.department_name = d.department_name;
