package config;

import com.opentable.db.postgres.embedded.EmbeddedPostgres;
import lombok.SneakyThrows;
import org.flywaydb.core.Flyway;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.sql.DataSource;

@Configuration
@ComponentScan(basePackages = "dao")
public class TestConfigurator {
    @Bean
    @SneakyThrows
    public DataSource dataSource() {
        return EmbeddedPostgres.builder().start().getPostgresDatabase();
    }

    @Bean(initMethod = "migrate", destroyMethod = "clean")
    public Flyway flyway() {
        return Flyway.configure()
                .locations("db/migration", "test/resources/db/migration")
                .dataSource(dataSource())
                .load();
    }

    @Bean
    public JdbcTemplate jdbcTemplate() {
        return new JdbcTemplate(dataSource());
    }

    @Bean
    public NamedParameterJdbcTemplate namedParameterJdbcTemplate() {
        return new NamedParameterJdbcTemplate(dataSource());
    }

}
