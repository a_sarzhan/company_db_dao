package dao.impl;

import entity.EmployeeAddress;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class EmployeeAddressDaoMockTest {
    private static final String INSERT_SQL = "INSERT INTO employee_address VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
    private static final String DELETE_SQL = "DELETE FROM employee_address WHERE address_id = ?";
    private static final String UPDATE_SQL = "UPDATE employee_address SET country = ?, state = ?, city = ?, street = ?, " +
            "apartment_no = ?, flat_no = ?, employee_id = ? WHERE address_id = ?";
    private static final String SELECT_BY_ID_SQL = "SELECT * FROM employee_address WHERE address_id = ?";
    private static final String SELECT_BY_EMPLOYEE_SQL = "SELECT * FROM employee_address WHERE employee_id = ?";

    @Mock
    private JdbcTemplate mockTemplate;
    @InjectMocks
    private EmployeeAddressDao mockDao;
    @Captor
    private ArgumentCaptor<Integer> idCaptor;
    @Captor
    private ArgumentCaptor<String> sqlCaptor;
    @Captor
    private ArgumentCaptor<Object> argsCaptor;
    @Captor
    private ArgumentCaptor<BeanPropertyRowMapper> mapperCaptor;

    @Test
    public void verifyArgumentsUsedInFindMethod() {
        mockDao.find(15);
        verify(mockTemplate, times(1)).queryForObject(sqlCaptor.capture(),
                (Object[]) argsCaptor.capture(), mapperCaptor.capture());
        List<Object> capturedId = Arrays.asList((Object[]) argsCaptor.getValue());
        assertEquals(SELECT_BY_ID_SQL, sqlCaptor.getValue());
        assertEquals(1, capturedId.size());
        assertEquals(15, capturedId.get(0));
        assertEquals(EmployeeAddress.class, mapperCaptor.getValue().getMappedClass());
    }

    @Test
    public void verifyFindAddressByEmployeeIdMethodContent() {
        mockDao.findAddressByEmployeeId(10051);
        verify(mockTemplate, times(1)).query(sqlCaptor.capture(),
                (Object[]) argsCaptor.capture(), mapperCaptor.capture());
        List<Object> capturedId = Arrays.asList((Object[]) argsCaptor.getValue());
        assertEquals(SELECT_BY_EMPLOYEE_SQL, sqlCaptor.getValue());
        assertEquals(1, capturedId.size());
        assertEquals(10051, capturedId.get(0));
        assertEquals(EmployeeAddress.class, mapperCaptor.getValue().getMappedClass());
    }

    @Test
    public void verifyArgumentsUsedInAddMethod() {
        EmployeeAddress testAddress = new EmployeeAddress(54, "Казахстан", "Карагандинская",
                "Караганда", "Чайковского", 27, 56, 10052);
        mockDao.add(testAddress);
        verify(mockTemplate, times(1)).update(sqlCaptor.capture(), (Object[]) argsCaptor.capture());
        List<Object> capturedArgs = argsCaptor.getAllValues();
        assertEquals(INSERT_SQL, sqlCaptor.getValue());
        assertEquals(8, capturedArgs.size());
        assertEquals(testAddress.getAddressId(), capturedArgs.get(0));
        assertEquals(testAddress.getCountry(), capturedArgs.get(1));
        assertEquals(testAddress.getState(), capturedArgs.get(2));
        assertEquals(testAddress.getCity(), capturedArgs.get(3));
        assertEquals(testAddress.getStreet(), capturedArgs.get(4));
        assertEquals(testAddress.getApartmentNo(), capturedArgs.get(5));
        assertEquals(testAddress.getFlatNo(), capturedArgs.get(6));
        assertEquals(testAddress.getEmployeeId(), capturedArgs.get(7));
    }

    @Test
    public void verifyArgumentsUsedInUpdateMethod() {
        EmployeeAddress testAddress = new EmployeeAddress(50, "Казахстан", "Атырауская",
                "Атырау", "Желтоксан", 5, 12, 10037);
        mockDao.update(testAddress);
        verify(mockTemplate, times(1)).update(sqlCaptor.capture(), (Object[]) argsCaptor.capture());
        List<Object> capturedArgs = argsCaptor.getAllValues();
        assertEquals(UPDATE_SQL, sqlCaptor.getValue());
        assertEquals(8, capturedArgs.size());
        assertEquals(testAddress.getCountry(), capturedArgs.get(0));
        assertEquals(testAddress.getState(), capturedArgs.get(1));
        assertEquals(testAddress.getCity(), capturedArgs.get(2));
        assertEquals(testAddress.getStreet(), capturedArgs.get(3));
        assertEquals(testAddress.getApartmentNo(), capturedArgs.get(4));
        assertEquals(testAddress.getFlatNo(), capturedArgs.get(5));
        assertEquals(testAddress.getEmployeeId(), capturedArgs.get(6));
        assertEquals(testAddress.getAddressId(), capturedArgs.get(7));
    }

    @Test
    public void verifyArgumentsUsedInDeleteMethod() {
        mockDao.delete(10);
        verify(mockTemplate, times(1)).update(sqlCaptor.capture(), idCaptor.capture());
        assertEquals(10, (int) idCaptor.getValue());
        assertEquals(DELETE_SQL, sqlCaptor.getValue());
    }
}
