package dao.impl;

import config.TestConfigurator;
import entity.Employee;
import lombok.SneakyThrows;
import org.flywaydb.core.Flyway;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.collection.IsIterableContainingInAnyOrder.containsInAnyOrder;
import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfigurator.class)
public class EmployeeDaoTest {
    @Autowired
    private EmployeeDao employeeDao;
    @Autowired
    private Flyway flyway;

    @Before
    public void setUp() throws Exception {
        flyway.migrate();
    }

    @After
    public void tearDown() throws Exception {
        flyway.clean();
    }

    @Test
    public void findGivenEmployeePass() {
        Employee foundEmployee = employeeDao.find(10000);
        assertEquals("Saltanat", foundEmployee.getFirstName());
        assertEquals("Bayzakova", foundEmployee.getLastName());
    }

    @SneakyThrows
    @Test
    public void addEmployeePass() {
        Date dob = new SimpleDateFormat("dd.MM.yyyy").parse("07.11.1993");
        Employee testEmployee = new Employee(20000, "Диас", "Мынбаев", dob, "male",
                new BigDecimal(350000), 10020, "KZ_DEP_DEVOP_PRJ_0001");
        assertTrue(employeeDao.add(testEmployee));
        assertNotNull(employeeDao.find(20000));
    }

    @SneakyThrows
    @Test(expected = DataIntegrityViolationException.class)
    public void addThrowsExceptionDueToInvalidDepartment() {
        Date dob = new SimpleDateFormat("dd.MM.yyyy").parse("07.11.1993");
        Employee testEmployee = new Employee(20000, "Диас", "Мынбаев", dob, "male",
                new BigDecimal(350000), 10020, "AAAAAAAA");
        employeeDao.add(testEmployee);
    }

    @SneakyThrows
    @Test
    public void addThenFindEmployeesByDepartmentPass() {
        List<Employee> actualList = employeeDao.findEmployeesByDepartment("KZ_DEP_DEV_APP_0003");
        assertTrue(actualList.size() > 0);
        assertThat(actualList, containsInAnyOrder(
                hasProperty("employeeId", is(10002)),
                hasProperty("employeeId", is(10007)),
                hasProperty("employeeId", is(10030)),
                hasProperty("employeeId", is(10032))));
    }

    @Test
    public void findEmployeeByDepartmentFail() {
        assertTrue(employeeDao.findEmployeesByDepartment("ABCDEFG").isEmpty());
    }

    @Test
    public void findEmployeesBySupervisorPass() {
        List<Employee> actualList = employeeDao.findEmployeesBySupervisor(10030);
        assertEquals(1, actualList.size());
        assertEquals(10002, actualList.get(0).getEmployeeId());
    }

    @Test
    public void findEmployeesBySupervisorFail() {
        assertTrue(employeeDao.findEmployeesBySupervisor(-100000).isEmpty());
    }

    @Test
    public void findEmployeeByNamePass() {
        List<Employee> actualList = employeeDao.findEmployeesByName("Maksim", "Galkin");
        assertTrue(actualList.size() > 0);
        assertThat(actualList, hasSize(1));
        assertEquals(10001, actualList.get(0).getEmployeeId());
    }

    @Test
    public void findEmployeeByNameFail() {
        List<Employee> actualList = employeeDao.findEmployeesByName("Maksim", "Perishkin");
        assertTrue(actualList.isEmpty());
    }

    @Test
    public void findAllShouldReturnFiftyTwoEmployees() {
        List<Employee> actualList = employeeDao.findAll();
        assertTrue(actualList.size() > 0);
        assertEquals(52, actualList.size());
    }

    @Test(expected = DataIntegrityViolationException.class)
    public void deleteEmployeeFail() {
        assertTrue(employeeDao.delete(10051));
    }

    @SneakyThrows
    @Test
    public void deleteEmployeePass() {
        Date dob = new SimpleDateFormat("dd.MM.yyyy").parse("07.11.1993");
        Employee testEmployee = new Employee(20000, "Диас", "Мынбаев", dob, "male",
                new BigDecimal(350000), 10020, "KZ_DEP_DEVOP_PRJ_0001");
        employeeDao.add(testEmployee);
        assertTrue(employeeDao.delete(testEmployee.getEmployeeId()));
        assertEquals(52, employeeDao.findAll().size());
    }

    @SneakyThrows
    @Test
    public void validateUpdatedEmployee() {
        Date dob = new SimpleDateFormat("dd.MM.yyyy").parse("05.07.1993");
        Employee existingEmployee = new Employee(10050, "Madina", "Botbayeva", dob, "female",
                new BigDecimal(450000.00), 10000, "KZ_DEP_DEV_APP_0002");
        Employee employeeToUpdate = new Employee(10050, "Madina", "Pirmatova", dob, "female",
                new BigDecimal(450000.00), 10020, "KZ_DEP_DEV_APP_0001");
        assertTrue(employeeDao.update(employeeToUpdate));
        Employee updatedEmployee = employeeDao.find(10050);
        assertEquals("Pirmatova", updatedEmployee.getLastName());
        assertEquals("KZ_DEP_DEV_APP_0001", updatedEmployee.getDepartment());
        assertNotEquals(existingEmployee.getSupervisorId(), updatedEmployee.getSupervisorId());
    }

    @Test
    public void validateUpdatedSupervisor() {
        int expectedSupervisorId = 10020;
        assertTrue(employeeDao.updateSupervisor(10050, expectedSupervisorId));
        Employee updatedEmployee = employeeDao.find(10050);
        assertEquals(expectedSupervisorId, updatedEmployee.getSupervisorId());
    }
}