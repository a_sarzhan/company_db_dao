package dao.impl;

import entity.Location;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class LocationDaoMockTest {
    private static final String FIND_SQL = "SELECT * FROM location WHERE location_id = ?";
    private static final String FIND_ALL_SQL = "SELECT * FROM location";
    private static final String FIND_BY_DEPARTMENT_SQL = "SELECT * FROM location WHERE department = ?";
    private static final String INSERT_SQL = "INSERT INTO location VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
    private static final String UPDATE_SQL = "UPDATE location SET country = ?, state_prov = ?, city = ?, " +
            "street = ?, apartment_no = ?, zip_code = ?, department = ? WHERE location_id = ?";
    private static final String DELETE_SQL = "DELETE FROM location WHERE location_id = ?";

    @Mock
    private JdbcTemplate mockTemplate;
    @InjectMocks
    private LocationDao mockDao;
    @Captor
    private ArgumentCaptor<String> sqlCaptor;
    @Captor
    private ArgumentCaptor<BeanPropertyRowMapper> mapperCaptor;
    @Captor
    private ArgumentCaptor<Object> argsCaptor;

    @Test
    public void testByMockingFindLocation() {
        mockDao.find(114);
        verify(mockTemplate, times(1)).queryForObject(sqlCaptor.capture(),
                (Object[]) argsCaptor.capture(), mapperCaptor.capture());
        List<Object> capturedArgs = Arrays.asList((Object[]) argsCaptor.getValue());
        assertEquals(FIND_SQL, sqlCaptor.getValue());
        assertEquals(1, argsCaptor.getAllValues().size());
        assertEquals(114, capturedArgs.get(0));
        assertEquals(Location.class, mapperCaptor.getValue().getMappedClass());
    }

    @Test
    public void testByMockingFindAllLocations() {
        mockDao.findAllLocations();
        verify(mockTemplate, times(1)).query(sqlCaptor.capture(), mapperCaptor.capture());
        assertEquals(FIND_ALL_SQL, sqlCaptor.getValue());
        assertEquals(Location.class, mapperCaptor.getValue().getMappedClass());
    }

    @Test
    public void testByMockingFindLocationsByDepartment() {
        mockDao.findLocationsByDepartment("KZ_DEP_DEV_APP_0003");
        verify(mockTemplate, times(1)).query(sqlCaptor.capture(),
                (Object[]) argsCaptor.capture(), mapperCaptor.capture());
        assertEquals(FIND_BY_DEPARTMENT_SQL, sqlCaptor.getValue());
        assertEquals(Location.class, mapperCaptor.getValue().getMappedClass());
        List<Object> capturedDepartment = Arrays.asList((Object[]) argsCaptor.getValue());
        assertEquals("KZ_DEP_DEV_APP_0003", capturedDepartment.get(0));
    }

    @Test
    public void testByMockingAddLocation() {
        Location testLocation = new Location(200, "Казахстан", null, "Караганда", "Майлина",
                77, "040001", "KZ_DEP_DEVOP_PRJ_0002");
        mockDao.add(testLocation);
        verify(mockTemplate, times(1)).update(sqlCaptor.capture(), argsCaptor.capture());
        assertEquals(INSERT_SQL, sqlCaptor.getValue());
        List<Object> capturedArgs = argsCaptor.getAllValues();
        assertEquals(8, capturedArgs.size());
        assertEquals(testLocation.getLocation_id(), capturedArgs.get(0));
        assertEquals(testLocation.getCountry(), capturedArgs.get(1));
        assertEquals(testLocation.getStateProv(), capturedArgs.get(2));
        assertEquals(testLocation.getCity(), capturedArgs.get(3));
        assertEquals(testLocation.getStreet(), capturedArgs.get(4));
        assertEquals(testLocation.getApartmentNo(), capturedArgs.get(5));
        assertEquals(testLocation.getZipCode(), capturedArgs.get(6));
        assertEquals(testLocation.getDepartment(), capturedArgs.get(7));
    }

    @Test
    public void testMyMockingUpdateLocation() {
        Location testLocation = new Location(115, "Казахстан", null, "Караганда", "Майлина",
                77, "040001", "KZ_DEP_DEVOP_PRJ_0002");
        mockDao.update(testLocation);
        verify(mockTemplate, times(1)).update(sqlCaptor.capture(), argsCaptor.capture());
        assertEquals(UPDATE_SQL, sqlCaptor.getValue());
        List<Object> capturedArgs = argsCaptor.getAllValues();
        assertEquals(8, capturedArgs.size());
        assertEquals(testLocation.getCountry(), capturedArgs.get(0));
        assertEquals(testLocation.getStateProv(), capturedArgs.get(1));
        assertEquals(testLocation.getCity(), capturedArgs.get(2));
        assertEquals(testLocation.getStreet(), capturedArgs.get(3));
        assertEquals(testLocation.getApartmentNo(), capturedArgs.get(4));
        assertEquals(testLocation.getZipCode(), capturedArgs.get(5));
        assertEquals(testLocation.getDepartment(), capturedArgs.get(6));
        assertEquals(testLocation.getLocation_id(), capturedArgs.get(7));
    }

    @Test
    public void testByMockingDeleteLocation() {
        mockDao.delete(115);
        verify(mockTemplate, times(1)).update(sqlCaptor.capture(), argsCaptor.capture());
        assertEquals(DELETE_SQL, sqlCaptor.getValue());
        assertEquals(115, argsCaptor.getValue());
    }

}