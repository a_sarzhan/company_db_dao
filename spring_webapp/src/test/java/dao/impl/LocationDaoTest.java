package dao.impl;

import config.TestConfigurator;
import entity.Location;
import org.flywaydb.core.Flyway;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfigurator.class)
public class LocationDaoTest {
    @Autowired
    private Flyway flyway;
    @Autowired
    private LocationDao locationDao;
    @Mock
    private JdbcTemplate mockTemplate;
    @InjectMocks
    private LocationDao mockDao;

    @Before
    public void setUp() throws Exception {
        flyway.migrate();
        MockitoAnnotations.initMocks(this);
    }

    @After
    public void tearDown() throws Exception {
        flyway.clean();
    }

    @Test
    public void shouldFindLocationIfLocationIdInScope() {
        Location expectedLocation = new Location(118, "Canada", "Quebec", "Montreal", "Rue Saint-Denis", 66, "K4C", "CA_DEP_PRJ_TEST_0002");
        Location actualLocation = locationDao.find(118);
        assertNotNull(actualLocation);
        assertEquals(expectedLocation, actualLocation);
    }

    @Test
    public void shouldFailToFindLocationIfLocationIdInvalid() {
        assertNull(locationDao.find(-10000));
    }

    @Test
    public void shouldSuccessToFindAllLocations() {
        Location hasLocation = new Location(118, "Canada", "Quebec", "Montreal",
                "Rue Saint-Denis", 66, "K4C", "CA_DEP_PRJ_TEST_0002");
        List<Location> allLocations = locationDao.findAllLocations();
        assertTrue(allLocations.size() > 0);
        assertEquals(19, allLocations.size());
        assertTrue(allLocations.contains(hasLocation));
    }

    @Test
    public void shouldFailToFindAllLocations() {
        List<Location> locations = new ArrayList<>();
        String FIND_ALL_SQL = "SELECT * FROM location";
        when(mockTemplate.query(FIND_ALL_SQL, BeanPropertyRowMapper.newInstance(Location.class))).thenReturn(locations);
        assertTrue(mockDao.findAllLocations().isEmpty());
    }

    @Test
    public void shouldFindListOfLocationsByDepartment() {
        String departmentName = "KZ_DEP_DEV_APP_0003";
        List<Location> locations = locationDao.findLocationsByDepartment(departmentName);
        assertFalse(locations.isEmpty());
        assertEquals(departmentName, locations.get(0).getDepartment());
    }

    @Test
    public void shouldFailToFindLocationsByDepartment() {
        String departmentName = "KZ_DEP_DEV_APP_0003";
        List<Location> locations = new ArrayList<>();
        String FIND_BY_DEPARTMENT_SQL = "SELECT * FROM location WHERE department = ?";
        ;
        when(mockTemplate.query(FIND_BY_DEPARTMENT_SQL, new String[]{departmentName},
                BeanPropertyRowMapper.newInstance(Location.class))).thenReturn(locations);
        assertTrue(mockDao.findAllLocations().isEmpty());
    }

    @Test
    public void shouldSuccessToAddLocation() {
        Location expectedLocation = new Location(300, "Kazakhstan", null, "Nur-Sultan",
                "Respublika", 55, "050001", "KZ_DEP_DEV_APP_0003");
        assertTrue(locationDao.add(expectedLocation));
        Location actualLocation = locationDao.find(300);
        assertNotNull(actualLocation);
        assertEquals(expectedLocation, actualLocation);
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowExceptionWhenAddInvokedIfLocationNull() {
        assertFalse(locationDao.add(null));
    }

    @Test(expected = DuplicateKeyException.class)
    public void shouldThrowExceptionWhenAddInvokedIfLocationExists() {
        Location expectedLocation = new Location(110, "Kazakhstan", null, "Nur-Sultan",
                "Respublika", 55, "050001", "KZ_DEP_DEV_APP_0003");
        assertFalse(locationDao.add(expectedLocation));
    }

    @Test
    public void shouldSuccessToUpdateLocation() {
        Location oldLocation = locationDao.find(110);
        Location newLocation = new Location(110, "Kazakhstan", null, "Nur-Sultan",
                "Respublika", 55, "050001", "KZ_DEP_DEV_APP_0003");
        assertTrue(locationDao.update(newLocation));
        Location actualLocation = locationDao.find(110);
        assertNotNull(actualLocation);
        assertNotEquals(oldLocation, actualLocation);
    }

    @Test
    public void shouldFailToUpdateLocationIfLocationIdInvalid() {
        Location testLocation = new Location(-300, "Kazakhstan", null, "Nur-Sultan",
                "Respublika", 55, "050001", "KZ_DEP_DEV_APP_0003");
        assertFalse(locationDao.update(testLocation));
    }

    @Test
    public void shouldSuccessToDeleteLocation() {
        Location testLocation = new Location(300, "Kazakhstan", null, "Nur-Sultan",
                "Respublika", 55, "050001", "KZ_DEP_DEV_APP_0003");
        locationDao.add(testLocation);
        assertTrue(locationDao.delete(300));
    }

    @Test
    public void shouldFailToDeleteLocationIfLocationIdInvalid() {
        assertFalse(locationDao.delete(-110));
    }
}