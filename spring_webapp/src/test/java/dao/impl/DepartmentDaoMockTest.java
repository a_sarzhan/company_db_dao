package dao.impl;

import dao.mapper.DepartmentRowMapper;
import entity.Department;
import entity.Employee;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class DepartmentDaoMockTest {
    private static final String FIND_SQL = "SELECT d.department_name, e.* FROM department d LEFT JOIN employee e " +
            "ON d.manager_id = e.employee_id WHERE d.department_name=:department";
    private static final String FIND_ALL_SQL = "SELECT d.department_name, e.* FROM department d LEFT JOIN employee e " +
            "ON d.manager_id = e.employee_id";
    private static final String FIND_DEPARTMENT_SQL = "SELECT d.department_name, e.* FROM department d LEFT JOIN employee e " +
            "ON d.manager_id = e.employee_id WHERE d.manager_id=:managerId LIMIT 1";
    private static final String INSERT_SQL = "INSERT INTO department VALUES (:department, :managerId)";
    private static final String UPDATE_DEPARTMENT_SQL = "UPDATE department SET department_name=:newDepartment, manager_id=:managerId WHERE department_name=:department";
    private static final String UPDATE_SQL = "UPDATE department SET manager_id=:managerId WHERE department_name=:department";
    private static final String DELETE_SQL = "DELETE FROM department WHERE department_name=:department";

    @Mock
    private NamedParameterJdbcTemplate mockTemplate;
    @Spy
    private MapSqlParameterSource spySqlParams;
    @InjectMocks
    private DepartmentDao mockDao;
    @Captor
    private ArgumentCaptor<String> sqlCaptor;
    @Captor
    private ArgumentCaptor<DepartmentRowMapper> mapperCaptor;
    @Captor
    private ArgumentCaptor<MapSqlParameterSource> argCaptor;

    @Test
    public void verifyFindDepartmentArguments() {
        mockDao.find("KZ_DEP_DEV_APP_0002");
        verify(mockTemplate, times(1)).queryForObject(sqlCaptor.capture(), argCaptor.capture(),
                mapperCaptor.capture());
        spySqlParams.addValue("department", "KZ_DEP_DEV_APP_0002");
        assertEquals(FIND_SQL, sqlCaptor.getValue());
        assertEquals(spySqlParams.getValues(), argCaptor.getValue().getValues());
        assertEquals(DepartmentRowMapper.class, mapperCaptor.getValue().getClass());
    }

    @Test
    public void verifyFindAllMethodArguments() {
        mockDao.findAll();
        verify(mockTemplate, times(1)).query(sqlCaptor.capture(), mapperCaptor.capture());
        assertEquals(FIND_ALL_SQL, sqlCaptor.getValue());
        assertEquals(DepartmentRowMapper.class, mapperCaptor.getValue().getClass());
    }

    @Test
    public void verifyFindByManagerArguments() {
        mockDao.findDepartmentByManager(10004);
        verify(mockTemplate, times(1)).queryForObject(sqlCaptor.capture(), argCaptor.capture(),
                mapperCaptor.capture());
        spySqlParams.addValue("managerId", 10004);
        assertEquals(FIND_DEPARTMENT_SQL, sqlCaptor.getValue());
        assertEquals(spySqlParams.getValues(), argCaptor.getValue().getValues());
        assertEquals(DepartmentRowMapper.class, mapperCaptor.getValue().getClass());
    }

    @Test
    public void verifyAddDepartmentArguments() {
        Employee testEmployee = new Employee(10060);
        Department testDepartment = new Department("KZ_TEST_DEPARTMENT", testEmployee, null);
        mockDao.add(testDepartment);
        verify(mockTemplate, times(1)).update(sqlCaptor.capture(), argCaptor.capture());
        spySqlParams.addValue("department", "KZ_TEST_DEPARTMENT").addValue("managerId", 10060);
        assertEquals(INSERT_SQL, sqlCaptor.getValue());
        assertEquals(spySqlParams.getValues(), argCaptor.getValue().getValues());
    }

    @Test
    public void verifyUpdateDepartmentArguments() {
        Employee testEmployee = new Employee(10060);
        Department testDepartment = new Department("KZ_TEST_DEPARTMENT", testEmployee, null);
        mockDao.update(testDepartment);
        verify(mockTemplate, times(1)).update(sqlCaptor.capture(), argCaptor.capture());
        spySqlParams.addValue("department", "KZ_TEST_DEPARTMENT").addValue("managerId", 10060);
        assertEquals(UPDATE_SQL, sqlCaptor.getValue());
        assertEquals(spySqlParams.getValues(), argCaptor.getValue().getValues());
    }

    @Test
    public void verifyUpdateDepartmentNameArguments() {
        Employee testEmployee = new Employee(10060);
        String oldName = "KZ_DEP_DEV_APP_0003";
        String newName = "KZ_TEST_DEPARTMENT";
        Department testDepartment = new Department(newName, testEmployee, null);
        mockDao.updateDepartmentName(oldName, testDepartment);
        verify(mockTemplate, times(1)).update(sqlCaptor.capture(), argCaptor.capture());
        spySqlParams.addValue("department", oldName)
                .addValue("managerId", 10060)
                .addValue("newDepartment", newName);
        assertEquals(UPDATE_DEPARTMENT_SQL, sqlCaptor.getValue());
        assertEquals(spySqlParams.getValues(), argCaptor.getValue().getValues());
    }

    @Test
    public void verifyDeleteDepartmentArguments() {
        String departmentName = "KZ_DEP_DEV_APP_0003";
        mockDao.delete(departmentName);
        verify(mockTemplate, times(1)).update(sqlCaptor.capture(), argCaptor.capture());
        spySqlParams.addValue("department", departmentName);
        assertEquals(DELETE_SQL, sqlCaptor.getValue());
        assertEquals(spySqlParams.getValues(), argCaptor.getValue().getValues());
    }
}