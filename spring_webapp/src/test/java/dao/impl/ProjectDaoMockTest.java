package dao.impl;

import dao.mapper.ProjectRowMapper;
import entity.Location;
import entity.Project;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class ProjectDaoMockTest {
    private static final String FIND_SQL = "SELECT p.project_name, p.project_no, l.* FROM project p LEFT JOIN location l " +
            "ON p.location_id = l.location_id WHERE p.project_name=:projectName";
    private static final String FIND_BY_LOCATION_SQL = "SELECT p.project_name, p.project_no, l.* FROM project p LEFT JOIN location l " +
            "ON p.location_id = l.location_id WHERE p.location_id=:locationId";
    private static final String FIND_ALL_SQL = "SELECT p.project_name, p.project_no, l.* FROM project p LEFT JOIN location l " +
            "ON p.location_id = l.location_id";
    private static final String FIND_BY_DEPARTMENT_SQL = "SELECT p.project_name, p.project_no, l.* FROM project p LEFT JOIN location l " +
            "ON p.location_id = l.location_id WHERE l.department=:departmentName";
    private static final String INSERT_SQL = "INSERT INTO project VALUES (:projectName, :projectNo, :locationId)";
    private static final String UPDATE_SQL = "UPDATE project SET project_no=:projectNo, location_id=:locationId " +
            "WHERE project_name=:projectName";
    private static final String UPDATE_PROJECT_NAME_SQL = "UPDATE project SET project_name=:newName WHERE project_name=:oldName";
    private static final String DELETE_SQL = "DELETE FROM project WHERE project_name=:projectName";
    @InjectMocks
    private ProjectDao mockDao;
    @Mock
    private NamedParameterJdbcTemplate mockTemplate;
    @Captor
    private ArgumentCaptor<String> sqlCaptor;
    @Captor
    private ArgumentCaptor<ProjectRowMapper> mapperCaptor;
    @Captor
    private ArgumentCaptor<MapSqlParameterSource> argCaptor;

    @Test
    public void shouldUseCorrectArguments_FindMethod() {
        String projectName = "KZ_JAVA_WEB_DEV_00004";
        mockDao.find(projectName);
        MapSqlParameterSource expectedParams = new MapSqlParameterSource("projectName", projectName);
        verify(mockTemplate, times(1)).queryForObject(sqlCaptor.capture(), argCaptor.capture(),
                mapperCaptor.capture());
        assertEquals(FIND_SQL, sqlCaptor.getValue());
        assertEquals(expectedParams.getValues(), argCaptor.getValue().getValues());
        assertEquals(ProjectRowMapper.class, mapperCaptor.getValue().getClass());
    }

    @Test
    public void shouldUseProperArguments_FindProjectsByLocation() {
        int testLocationId = 114;
        mockDao.findProjectsByLocation(testLocationId);
        MapSqlParameterSource expectedParams = new MapSqlParameterSource("locationId", testLocationId);
        verify(mockTemplate, times(1)).query(sqlCaptor.capture(), argCaptor.capture(), mapperCaptor.capture());
        assertEquals(FIND_BY_LOCATION_SQL, sqlCaptor.getValue());
        assertEquals(expectedParams.getValues(), argCaptor.getValue().getValues());
        assertEquals(ProjectRowMapper.class, mapperCaptor.getValue().getClass());
    }

    @Test
    public void shouldUseProperArguments_FindProjectsByDepartment() {
        String testDepartment = "KZ_DEP_DEVOP_PRJ_0002";
        mockDao.findProjectsByDepartment(testDepartment);
        MapSqlParameterSource expectedParams = new MapSqlParameterSource("departmentName", testDepartment);
        verify(mockTemplate, times(1)).query(sqlCaptor.capture(), argCaptor.capture(), mapperCaptor.capture());
        assertEquals(FIND_BY_DEPARTMENT_SQL, sqlCaptor.getValue());
        assertEquals(expectedParams.getValues(), argCaptor.getValue().getValues());
        assertEquals(ProjectRowMapper.class, mapperCaptor.getValue().getClass());
    }

    @Test
    public void shouldUseProperArguments_FindAllProjects() {
        mockDao.findAll();
        verify(mockTemplate, times(1)).query(sqlCaptor.capture(), mapperCaptor.capture());
        assertEquals(FIND_ALL_SQL, sqlCaptor.getValue());
        assertEquals(ProjectRowMapper.class, mapperCaptor.getValue().getClass());
    }

    @Test
    public void shouldUseProperArguments_AddProject() {
        Project testProject = new Project("KZ_JAVA_MOB_DEV_00010", 33, new Location(200));
        mockDao.add(testProject);
        MapSqlParameterSource expectedParams = new MapSqlParameterSource();
        expectedParams.addValue("projectName", testProject.getProjectName())
                .addValue("projectNo", testProject.getProjectNo())
                .addValue("locationId", testProject.getLocation().getLocation_id());
        verify(mockTemplate, times(1)).update(sqlCaptor.capture(), argCaptor.capture());
        assertEquals(INSERT_SQL, sqlCaptor.getValue());
        assertEquals(expectedParams.getValues(), argCaptor.getValue().getValues());
    }

    @Test
    public void shouldUseProperArguments_UpdateProject() {
        Project testProject = new Project("KZ_JAVA_MOB_DEV_00010", 33, new Location(200));
        mockDao.update(testProject);
        MapSqlParameterSource expectedParams = new MapSqlParameterSource();
        expectedParams.addValue("projectName", testProject.getProjectName())
                .addValue("projectNo", testProject.getProjectNo())
                .addValue("locationId", testProject.getLocation().getLocation_id());
        verify(mockTemplate, times(1)).update(sqlCaptor.capture(), argCaptor.capture());
        assertEquals(UPDATE_SQL, sqlCaptor.getValue());
        assertEquals(expectedParams.getValues(), argCaptor.getValue().getValues());
    }

    @Test
    public void shouldUseProperArguments_UpdateProjectName() {
        String oldProjectName = "KZ_JAVA_MOB_DEV_00008";
        String newProjectName = "KZ_JAVA_MOB_DEV_00011";
        mockDao.updateProjectName(oldProjectName, newProjectName);
        MapSqlParameterSource expectedParams = new MapSqlParameterSource();
        expectedParams.addValue("newName", newProjectName).addValue("oldName", oldProjectName);
        verify(mockTemplate, times(1)).update(sqlCaptor.capture(), argCaptor.capture());
        assertEquals(UPDATE_PROJECT_NAME_SQL, sqlCaptor.getValue());
        assertEquals(expectedParams.getValues(), argCaptor.getValue().getValues());
    }

    @Test
    public void shouldUseProperArguments_DeleteProject() {
        String projectName = "KZ_JAVA_WEB_DEV_00004";
        mockDao.delete(projectName);
        MapSqlParameterSource expectedParams = new MapSqlParameterSource("projectName", projectName);
        verify(mockTemplate, times(1)).update(sqlCaptor.capture(), argCaptor.capture());
        assertEquals(DELETE_SQL, sqlCaptor.getValue());
        assertEquals(expectedParams.getValues(), argCaptor.getValue().getValues());
    }
}