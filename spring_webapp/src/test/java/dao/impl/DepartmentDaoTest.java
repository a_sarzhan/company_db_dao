package dao.impl;

import config.TestConfigurator;
import dao.mapper.DepartmentRowMapper;
import entity.Department;
import entity.Employee;
import entity.Location;
import org.flywaydb.core.Flyway;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfigurator.class)
public class DepartmentDaoTest {
    @Autowired
    private Flyway flyway;
    @Autowired
    private DepartmentDao dao;
    @Mock
    private JdbcTemplate mockTemplate;
    @InjectMocks
    private LocationDao mockDao;

    @Before
    public void setUp() throws Exception {
        flyway.migrate();
        MockitoAnnotations.initMocks(this);
    }

    @After
    public void tearDown() throws Exception {
        flyway.clean();
    }

    @Test
    public void shouldSuccessToFindDepartment() {
        String departmentName = "KZ_DEP_DEVOP_PRJ_0002";
        Department actualDepartment = dao.find(departmentName);
        assertNotNull(actualDepartment);
        assertEquals(departmentName, actualDepartment.getDepartmentName());
    }

    @Test
    public void shouldFailToFindIfDepartmentNameInvalid() {
        String departmentName = "ABCDEFG";
        assertNull(dao.find(departmentName));
    }

    @Test
    public void shouldSuccessToFindAllDepartments() {
        List<Department> departments = dao.findAll();
        assertTrue(departments.size() > 0);
    }

    @Test
    public void shouldFailToFindAllDepartments() {
        List<Department> departments = new ArrayList<>();
        String FIND_ALL_SQL = "SELECT d.department_name, e.* FROM department d LEFT JOIN employee e " +
                "ON d.manager_id = e.employee_id";
        when(mockTemplate.query(FIND_ALL_SQL, new DepartmentRowMapper())).thenReturn(departments);
        assertTrue(mockDao.findAllLocations().isEmpty());
    }

    @Test
    public void shouldSuccessToFindDepartmentByManager() {
        int managerId = 10000;
        Department actualDepartment = dao.findDepartmentByManager(managerId);
        assertNotNull(actualDepartment);
        assertEquals(managerId, actualDepartment.getManager().getEmployeeId());
    }

    @Test
    public void shouldFailToFindDepartmentIfManagerIdInvalid() {
        int managerId = -10005;
        assertNull(dao.findDepartmentByManager(managerId));
    }

    @Test
    public void shouldSuccessToAddDepartmentIfDepartmentIsNew() {
        Department expectedDepartment = new Department("KZ_DEP_DEVOP_PRJ_0012", new Employee(10006), null);
        assertTrue(dao.add(expectedDepartment));
        Department actualDepartment = dao.find("KZ_DEP_DEVOP_PRJ_0012");
        assertNotNull(actualDepartment);
        assertEquals(expectedDepartment.getDepartmentName(), actualDepartment.getDepartmentName());
    }

    @Test(expected = DuplicateKeyException.class)
    public void shouldThrowExceptionWhenAddInvokedIfDepartmentExists() {
        Department expectedDepartment = new Department("KZ_DEP_DEVOP_PRJ_0002", new Employee(10006), null);
        dao.add(expectedDepartment);
    }

    @Test
    public void shouldSuccessToUpdateDepartment() {
        Department oldDepartment = dao.find("KZ_DEP_DEVOP_PRJ_0002");
        Department newDepartment = new Department("KZ_DEP_DEVOP_PRJ_0002", new Employee(10007), null);
        assertTrue(dao.update(newDepartment));
        Department actualDepartment = dao.find("KZ_DEP_DEVOP_PRJ_0002");
        assertNotNull(actualDepartment);
        assertNotEquals(oldDepartment.getManager().getEmployeeId(), actualDepartment.getManager().getEmployeeId());
    }

    @Test
    public void shouldFailToUpdateDepartmentIfDepartmentNameNotExist() {
        Department newDepartment = new Department("ABCDEFG", new Employee(10007), null);
        assertFalse(dao.update(newDepartment));
    }

    @Test
    public void shouldSuccessToUpdateDepartmentName() {
        String oldName = "KZ_DEP_DEVOP_PRJ_0002";
        Department newDepartment = new Department("KZ_DEP_DEVOP_PRJ_0012", new Employee(10007), null);
        assertTrue(dao.updateDepartmentName(oldName, newDepartment));
    }

    @Test
    public void shouldFailToUpdateDepartmentNameIfNameNotExist() {
        String oldName = "ABCDEFG";
        Department newDepartment = new Department("KZ_DEP_DEVOP_PRJ_0002", new Employee(10007), null);
        assertFalse(dao.updateDepartmentName(oldName, newDepartment));
    }

    @Test
    public void shouldSuccessToDeleteDepartment() {
        Department testDepartment = new Department("KZ_DEP_DEVOP_PRJ_0012", new Employee(10007), null);
        dao.add(testDepartment);
        assertTrue(dao.delete(testDepartment.getDepartmentName()));
    }

    @Test
    public void shouldFailToDeleteDepartmentIfDepartmentNameInvalid() {
        assertFalse(dao.delete("isdscpsvk"));
    }
}