package dao.impl;

import config.TestConfigurator;
import entity.Employee;
import lombok.SneakyThrows;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class EmployeeDaoMockTest {
    private static final String FIND_EMPLOYEE_SQL = "SELECT * FROM employee where employee_id=:employeeId";
    private static final String FIND_BY_DEPARTMENT_SQL = "SELECT * FROM employee where department=:department";
    private static final String FIND_BY_SUPERVISOR_SQL = "SELECT * FROM employee where supervisor_id=:supervisor";
    private static final String FIND_BY_NAME_SQL = "SELECT * FROM employee where first_name=:firstName and last_name=:lastName";
    private static final String FIND_ALL_SQL = "SELECT * FROM employee";
    private static final String INSERT_SQL = "INSERT INTO employee VALUES (:employeeId, :name, :lastName, :birthDate, :gender, " +
            ":salary, :supervisor, :department)";
    private static final String UPDATE_SQL = "UPDATE employee SET first_name=:name, last_name=:lastName, birth_date=:birthDate," +
            "gender=:gender, salary=:salary, supervisor_id=:supervisor, department=:department WHERE employee_id=:employeeId";
    private static final String UPDATE_SUPERVISOR_SQL = "UPDATE employee SET supervisor_id=:supervisor WHERE employee_id=:employeeId";
    private static final String DELETE_SQL = "DELETE FROM employee WHERE employee_id=:employeeId";

    @Mock
    private NamedParameterJdbcTemplate mockTemplate;
    @Spy
    private MapSqlParameterSource spyParams;
    @InjectMocks
    private EmployeeDao mockDao;
    @Captor
    private ArgumentCaptor<String> sqlCaptor;
    @Captor
    private ArgumentCaptor<BeanPropertyRowMapper> mapperCaptor;
    @Captor
    private ArgumentCaptor<MapSqlParameterSource> argCaptor;

    @Test
    public void testFindMethodContent() {
        mockDao.find(10050);
        spyParams.addValue("employeeId", 10050);
        verify(mockTemplate, times(1)).queryForObject(sqlCaptor.capture(),
                argCaptor.capture(), mapperCaptor.capture());
        assertEquals(FIND_EMPLOYEE_SQL, sqlCaptor.getValue());
        assertEquals(spyParams.getValues(), argCaptor.getValue().getValues());
        assertEquals(Employee.class, mapperCaptor.getValue().getMappedClass());
    }

    @Test
    public void testFindEmployeesByDepartmentMethodContent() {
        String testDepartment = "KZ_DEP_DEV_APP_0002";
        mockDao.findEmployeesByDepartment(testDepartment);
        spyParams.addValue("department", testDepartment);
        verify(mockTemplate, times(1)).query(sqlCaptor.capture(), argCaptor.capture(),
                mapperCaptor.capture());
        assertEquals(FIND_BY_DEPARTMENT_SQL, sqlCaptor.getValue());
        assertEquals(spyParams.getValues(), argCaptor.getValue().getValues());
        assertEquals(Employee.class, mapperCaptor.getValue().getMappedClass());
    }

    @Test
    public void testFindEmployeesBySupervisorMethodContent() {
        int testSupervisorId = 10000;
        mockDao.findEmployeesBySupervisor(testSupervisorId);
        spyParams.addValue("supervisor", testSupervisorId);
        verify(mockTemplate, times(1)).query(sqlCaptor.capture(), argCaptor.capture(),
                mapperCaptor.capture());
        assertEquals(FIND_BY_SUPERVISOR_SQL, sqlCaptor.getValue());
        assertEquals(spyParams.getValues(), argCaptor.getValue().getValues());
        assertEquals(Employee.class, mapperCaptor.getValue().getMappedClass());
    }

    @Test
    public void testFindEmployeesByNameMethodContent() {
        mockDao.findEmployeesByName("Vladimir", "Petrov");
        spyParams.addValue("firstName", "Vladimir")
                .addValue("lastName", "Petrov");
        verify(mockTemplate, times(1)).query(sqlCaptor.capture(), argCaptor.capture(),
                mapperCaptor.capture());
        assertEquals(FIND_BY_NAME_SQL, sqlCaptor.getValue());
        assertEquals(spyParams.getValues(), argCaptor.getValue().getValues());
        assertEquals(Employee.class, mapperCaptor.getValue().getMappedClass());
    }

    @Test
    public void testFindAllMethodContent() {
        mockDao.findAll();
        verify(mockTemplate, times(1)).query(sqlCaptor.capture(), mapperCaptor.capture());
        assertEquals(FIND_ALL_SQL, sqlCaptor.getValue());
        assertEquals(Employee.class, mapperCaptor.getValue().getMappedClass());
    }

    @SneakyThrows
    @Test
    public void testAddMethodContent() {
        Employee testEmployee = new Employee(20000, "Sandra", "Bullock", new SimpleDateFormat("yyyy-MM-dd").parse("1994-09-09"),
                "female", new BigDecimal("500000.00"), 10044, "KZ_DEP_DEVOP_PRJ_0002");
        mockDao.add(testEmployee);
        verify(mockTemplate, times(1)).update(sqlCaptor.capture(), argCaptor.capture());
        assertEquals(INSERT_SQL, sqlCaptor.getValue());
        assertEquals(getSpiedSqlParams(testEmployee).getValues(), argCaptor.getValue().getValues());
    }

    @SneakyThrows
    @Test
    public void testUpdateMethodContent() {
        Employee testEmployee = new Employee(20000, "Sandra", "Bullock", new SimpleDateFormat("yyyy-MM-dd").parse("1994-09-09"),
                "female", new BigDecimal("500000.00"), 10044, "KZ_DEP_DEVOP_PRJ_0002");
        mockDao.update(testEmployee);
        verify(mockTemplate, times(1)).update(sqlCaptor.capture(), argCaptor.capture());
        assertEquals(UPDATE_SQL, sqlCaptor.getValue());
        assertEquals(getSpiedSqlParams(testEmployee).getValues(), argCaptor.getValue().getValues());
    }

    @Test
    public void testUpdateSupervisorMethodContent() {
        int supervisorId = 10005;
        int employeeId = 10030;
        mockDao.updateSupervisor(employeeId, supervisorId);
        spyParams.addValue("employeeId", employeeId).addValue("supervisor", supervisorId);
        verify(mockTemplate, times(1)).update(sqlCaptor.capture(), argCaptor.capture());
        assertEquals(UPDATE_SUPERVISOR_SQL, sqlCaptor.getValue());
        assertEquals(spyParams.getValues(), argCaptor.getValue().getValues());
    }

    @Test
    public void testDeleteMethodContent() {
        int employeeId = 10040;
        mockDao.delete(employeeId);
        spyParams.addValue("employeeId", employeeId);
        verify(mockTemplate, times(1)).update(sqlCaptor.capture(), argCaptor.capture());
        assertEquals(DELETE_SQL, sqlCaptor.getValue());
        assertEquals(spyParams.getValues(), argCaptor.getValue().getValues());
    }

    private MapSqlParameterSource getSpiedSqlParams(Employee testEmployee) {
        return spyParams.addValue("employeeId", testEmployee.getEmployeeId())
                .addValue("name", testEmployee.getFirstName())
                .addValue("lastName", testEmployee.getLastName())
                .addValue("birthDate", testEmployee.getBirthDate())
                .addValue("gender", testEmployee.getGender())
                .addValue("salary", testEmployee.getSalary())
                .addValue("supervisor", testEmployee.getSupervisorId())
                .addValue("department", testEmployee.getDepartment());
    }

}
