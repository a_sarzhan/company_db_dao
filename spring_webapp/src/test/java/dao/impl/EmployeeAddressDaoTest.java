package dao.impl;

import config.TestConfigurator;
import entity.EmployeeAddress;
import org.flywaydb.core.Flyway;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfigurator.class)
public class EmployeeAddressDaoTest {
    @Autowired
    private EmployeeAddressDao addressDao;
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private Flyway flyway;

    @Before
    public void setUp() throws Exception {
        flyway.migrate();
    }

    @After
    public void tearDown() throws Exception {
        flyway.clean();
    }

    @Test
    public void shouldReturnEmployeeAddressByGivenId() {
        EmployeeAddress expectedAddress = new EmployeeAddress(52, "Kazakhstan", "Akmolinskaya",
                "Kokshetau", "Kenesary", 14, 12, 10051);
        EmployeeAddress actualAddress = addressDao.find(52);
        assertNotNull(actualAddress);
        assertEquals(expectedAddress, actualAddress);
    }

    @Test
    public void shouldReturnNullIfAddressNotFound() {
        EmployeeAddress testAddress = addressDao.find(-10000);
        assertNull(testAddress);
    }

    @Test
    public void findAddressesByGivenEmployeeIdPass() {
        List<EmployeeAddress> employeeAddresses = addressDao.findAddressByEmployeeId(10051);
        assertNotNull(employeeAddresses);
        assertTrue(employeeAddresses.size() > 0);
    }

    @Test
    public void findAddressesByGivenEmployeeIdFail() {
        List<EmployeeAddress> employeeAddresses = addressDao.findAddressByEmployeeId(-10051);
        assertEquals(0, employeeAddresses.size());
    }

    @Test
    public void shouldSuccessfullyAddAddress() {
        EmployeeAddress testAddress = new EmployeeAddress(54, "Казахстан", "Карагандинская",
                "Караганда", "Чайковского", 27, 56, 10050);
        assertTrue(addressDao.add(testAddress));
        EmployeeAddress resultAddress = addressDao.find(54);
        assertEquals("Караганда", resultAddress.getCity());
        assertEquals("Чайковского", resultAddress.getStreet());
        assertEquals(10050, resultAddress.getEmployeeId());
    }

    @Test
    public void shouldSuccessfullyUpdateAddress() {
        EmployeeAddress newAddress = new EmployeeAddress(55, "Казахстан", "Атырауская",
                "Атырау", "Желтоксан", 5, 12, 10037);
        addressDao.add(newAddress);
        EmployeeAddress updateAddress = new EmployeeAddress(55, "Казахстан", "Алматинская",
                "Талдыкорган", "Желтоксан", 55, 12, 10037);
        assertTrue(addressDao.update(updateAddress));
        EmployeeAddress actualAddress = addressDao.find(55);
        assertNotEquals("Атырауская", actualAddress.getState());
        assertNotEquals("Атырау", actualAddress.getCity());
        assertNotEquals(5, actualAddress.getApartmentNo());
    }

    @Test
    public void shouldReturnLessSizeAfterDelete() {
        EmployeeAddress newAddress = new EmployeeAddress(55, "Казахстан", "Атырауская",
                "Атырау", "Желтоксан", 5, 12, 10037);
        addressDao.add(newAddress);
        assertEquals("Атырау", addressDao.find(55).getCity());
        addressDao.delete(55);
        assertNull(addressDao.find(55));
    }
}