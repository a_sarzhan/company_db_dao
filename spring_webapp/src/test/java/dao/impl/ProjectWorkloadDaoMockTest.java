package dao.impl;

import entity.ProjectWorkload;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class ProjectWorkloadDaoMockTest {
    private static final String FIND_SQL = "SELECT * FROM project_workload WHERE project_name=:projectName " +
            "AND employee_id=:employeeId";
    private static final String FIND_BY_PROJECT_SQL = "SELECT * FROM project_workload WHERE project_name=:projectName";
    private static final String FIND_BY_EMPLOYEE_SQL = "SELECT * FROM project_workload WHERE employee_id=:employeeId";
    private static final String INSERT_SQL = "INSERT INTO project_workload VALUES (:projectName, :employeeId, " +
            ":workHoursWeek)";
    private static final String UPDATE_SQL = "UPDATE project_workload SET work_hours_week=:workHoursWeek " +
            "WHERE project_name=:projectName AND employee_id=:employeeId";
    private static final String DELETE_SINGLE_SQL = "DELETE FROM project_workload WHERE project_name=:projectName " +
            "AND employee_id=:employeeId";
    private static final String DELETE_BY_PROJECT_SQL = "DELETE FROM project_workload WHERE project_name=:projectName";

    @Mock
    private NamedParameterJdbcTemplate mockTemplate;
    @InjectMocks
    private ProjectWorkloadDao mockDao;
    @Captor
    private ArgumentCaptor<String> sqlCaptor;
    @Captor
    private ArgumentCaptor<BeanPropertyRowMapper> mapperCaptor;
    @Captor
    private ArgumentCaptor<MapSqlParameterSource> argCaptor;

    @Test
    public void shouldUseValidArguments_FindWorkload() {
        String projectName = "KZ_JAVA_MOB_DEV_00009";
        int employeeId = 10013;
        MapSqlParameterSource expectedParams = new MapSqlParameterSource("projectName", projectName)
                .addValue("employeeId", employeeId);
        mockDao.find(projectName, employeeId);
        verify(mockTemplate, times(1)).queryForObject(sqlCaptor.capture(), argCaptor.capture(),
                mapperCaptor.capture());
        assertEquals(FIND_SQL, sqlCaptor.getValue());
        assertEquals(expectedParams.getValues(), argCaptor.getValue().getValues());
        assertEquals(ProjectWorkload.class, mapperCaptor.getValue().getMappedClass());
    }

    @Test
    public void shouldUseValidArguments_FindWorkloadsByProjectName() {
        String projectName = "KZ_JAVA_MOB_DEV_00009";
        MapSqlParameterSource expectedParams = new MapSqlParameterSource("projectName", projectName);
        mockDao.findProjectWorkloadByProject(projectName);
        verify(mockTemplate, times(1)).query(sqlCaptor.capture(), argCaptor.capture(),
                mapperCaptor.capture());
        assertEquals(FIND_BY_PROJECT_SQL, sqlCaptor.getValue());
        assertEquals(expectedParams.getValues(), argCaptor.getValue().getValues());
        assertEquals(ProjectWorkload.class, mapperCaptor.getValue().getMappedClass());
    }

    @Test
    public void shouldUseValidArguments_FindWorkloadsByEmployee() {
        int employeeId = 10013;
        MapSqlParameterSource expectedParams = new MapSqlParameterSource("employeeId", employeeId);
        mockDao.findProjectWorkloadByEmployee(employeeId);
        verify(mockTemplate, times(1)).query(sqlCaptor.capture(), argCaptor.capture(),
                mapperCaptor.capture());
        assertEquals(FIND_BY_EMPLOYEE_SQL, sqlCaptor.getValue());
        assertEquals(expectedParams.getValues(), argCaptor.getValue().getValues());
        assertEquals(ProjectWorkload.class, mapperCaptor.getValue().getMappedClass());
    }

    @Test
    public void shouldUseValidArguments_AddProjectWorkload() {
        ProjectWorkload testWorkload = new ProjectWorkload("KZ_JAVA_MOB_DEV_00009", 10014, 24.5);
        MapSqlParameterSource expectedParams = new MapSqlParameterSource("projectName", testWorkload.getProjectName());
        expectedParams.addValue("employeeId", testWorkload.getEmployeeId());
        expectedParams.addValue("workHoursWeek", testWorkload.getWorkHoursWeek());
        mockDao.add(testWorkload);
        verify(mockTemplate, times(1)).update(sqlCaptor.capture(), argCaptor.capture());
        assertEquals(INSERT_SQL, sqlCaptor.getValue());
        assertEquals(expectedParams.getValues(), argCaptor.getValue().getValues());
    }

    @Test
    public void shouldUseValidArguments_UpdateProjectWorkload() {
        ProjectWorkload testWorkload = new ProjectWorkload("KZ_JAVA_MOB_DEV_00009", 10014, 24.5);
        MapSqlParameterSource expectedParams = new MapSqlParameterSource("projectName", testWorkload.getProjectName());
        expectedParams.addValue("employeeId", testWorkload.getEmployeeId());
        expectedParams.addValue("workHoursWeek", testWorkload.getWorkHoursWeek());
        mockDao.update(testWorkload);
        verify(mockTemplate, times(1)).update(sqlCaptor.capture(), argCaptor.capture());
        assertEquals(UPDATE_SQL, sqlCaptor.getValue());
        assertEquals(expectedParams.getValues(), argCaptor.getValue().getValues());
    }

    @Test
    public void shouldUseValidArguments_DeleteSingleWorkload() {
        String projectName = "KZ_JAVA_MOB_DEV_00009";
        int employeeId = 10013;
        MapSqlParameterSource expectedParams = new MapSqlParameterSource("projectName", projectName)
                .addValue("employeeId", employeeId);
        mockDao.deleteSingleProjectWorkload(projectName, employeeId);
        verify(mockTemplate, times(1)).update(sqlCaptor.capture(), argCaptor.capture());
        assertEquals(DELETE_SINGLE_SQL, sqlCaptor.getValue());
        assertEquals(expectedParams.getValues(), argCaptor.getValue().getValues());
    }

    @Test
    public void shouldUseValidArguments_DeleteWorkloadsByProject() {
        String projectName = "KZ_JAVA_MOB_DEV_00009";
        MapSqlParameterSource expectedParams = new MapSqlParameterSource("projectName", projectName);
        mockDao.deleteWorkLoadByProject(projectName);
        verify(mockTemplate, times(1)).update(sqlCaptor.capture(), argCaptor.capture());
        assertEquals(DELETE_BY_PROJECT_SQL, sqlCaptor.getValue());
        assertEquals(expectedParams.getValues(), argCaptor.getValue().getValues());
    }
}