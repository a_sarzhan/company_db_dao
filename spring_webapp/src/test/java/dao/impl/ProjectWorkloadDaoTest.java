package dao.impl;

import config.TestConfigurator;
import entity.Location;
import entity.Project;
import entity.ProjectWorkload;
import org.flywaydb.core.Flyway;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfigurator.class)
public class ProjectWorkloadDaoTest {
    @Autowired
    private Flyway flyway;
    @Autowired
    private ProjectWorkloadDao dao;

    @Before
    public void setUp() throws Exception {
        flyway.migrate();
    }

    @After
    public void tearDown() throws Exception {
        flyway.clean();
    }

    @Test
    public void shouldSuccessToFindWorkload() {
        ProjectWorkload workload = dao.find("KZ_JAVA_MOB_DEV_00003", 10006);
        assertNotNull(workload);
        assertEquals("KZ_JAVA_MOB_DEV_00003", workload.getProjectName());
        assertEquals(10006, workload.getEmployeeId());
    }

    @Test
    public void shouldFailToFindWorkload() {
        ProjectWorkload workload = dao.find("KZ_JAVA_MOB_DEV_00053", -10006);
        assertNull(workload);
    }

    @Test
    public void shouldSuccessToFindWorkloadListByProjectName() {
        List<ProjectWorkload> workloads = dao.findProjectWorkloadByProject("KZ_JAVA_MOB_DEV_00003");
        assertFalse(workloads.isEmpty());
        assertEquals("KZ_JAVA_MOB_DEV_00003", workloads.get(0).getProjectName());
    }

    @Test
    public void shouldFailToFindWorkloadIfProjectNameInvalid() {
        List<ProjectWorkload> workloads = dao.findProjectWorkloadByProject("ABCDEFG");
        assertTrue(workloads.isEmpty());
    }

    @Test
    public void shouldSuccessToFindWorkloadListByEmployee() {
        List<ProjectWorkload> workloads = dao.findProjectWorkloadByEmployee(10000);
        assertFalse(workloads.isEmpty());
        assertEquals(10000, workloads.get(0).getEmployeeId());
    }

    @Test
    public void shouldFailToFindWorkloadIfEmployeeInvalid() {
        List<ProjectWorkload> workloads = dao.findProjectWorkloadByEmployee(-10003);
        assertTrue(workloads.isEmpty());
    }

    @Test
    public void shouldSuccessToAddWorkloadIfValuesValid() {
        ProjectWorkload workload = new ProjectWorkload("KZ_JAVA_MOB_DEV_00003", 10009, 18.8);
        assertTrue(dao.add(workload));
    }

    @Test(expected = DataIntegrityViolationException.class)
    public void shouldFailToAddWorkloadIfValuesInvalid() {
        ProjectWorkload workload = new ProjectWorkload("ABCDEFG", 10009, 18.8);
        dao.add(workload);
    }

    @Test
    public void shouldSuccessToUpdateProjectWorkload() {
        ProjectWorkload newWorkload = new ProjectWorkload("KZ_JAVA_MOB_DEV_00003", 10006, 17.33);
        assertTrue(dao.update(newWorkload));
        ProjectWorkload actualWorkload = dao.find("KZ_JAVA_MOB_DEV_00003", 10006);
        assertNotNull(actualWorkload);
        assertEquals(newWorkload, actualWorkload);
    }

    @Test
    public void shouldFailToUpdateProjectWorkloadIfCompositeKeyInvalid() {
        ProjectWorkload newWorkload = new ProjectWorkload("KZ_JAVA_MOB_DEV_00003", 10009, 17.33);
        assertFalse(dao.update(newWorkload));
    }

    @Test
    public void shouldSuccessToDeleteWorkloadIfCompositeKeysAreValid() {
        dao.deleteSingleProjectWorkload("KZ_JAVA_MOB_DEV_00003", 10006);
        assertNull(dao.find("KZ_JAVA_MOB_DEV_00003", 10006));
    }

    @Test
    public void shouldSuccessToDeleteListOfWorkloadByProjectName() {
        dao.deleteWorkLoadByProject("KZ_JAVA_MOB_DEV_00003");
        List<ProjectWorkload> workloads = dao.findProjectWorkloadByProject("KZ_JAVA_MOB_DEV_00003");
        assertTrue(workloads.isEmpty());
    }
}