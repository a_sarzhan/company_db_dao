package dao.impl;

import config.TestConfigurator;
import dao.mapper.DepartmentRowMapper;
import dao.mapper.ProjectRowMapper;
import entity.Department;
import entity.Employee;
import entity.Location;
import entity.Project;
import org.flywaydb.core.Flyway;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfigurator.class)
public class ProjectDaoTest {
    @Autowired
    private Flyway flyway;
    @Autowired
    private ProjectDao dao;
    @Mock
    private NamedParameterJdbcTemplate mockTemplate;
    @InjectMocks
    private ProjectDao mockDao;


    @Before
    public void setUp() throws Exception {
        flyway.migrate();
        MockitoAnnotations.initMocks(this);
    }

    @After
    public void tearDown() throws Exception {
        flyway.clean();
    }

    @Test
    public void shouldSuccessToFindProjectByProjectName() {
        Project actualProject = dao.find("KZ_JAVA_WEB_DEV_00006");
        assertNotNull(actualProject);
        assertEquals("KZ_JAVA_WEB_DEV_00006", actualProject.getProjectName());
    }

    @Test
    public void shouldFailToFindProjectByProjectName() {
        String FIND_SQL = "SELECT p.project_name, p.project_no, l.* FROM project p LEFT JOIN location l " +
                "ON p.location_id = l.location_id WHERE p.project_name=:projectName";
        when(mockTemplate.queryForObject(FIND_SQL, new MapSqlParameterSource(), new ProjectRowMapper()))
                .thenThrow(EmptyResultDataAccessException.class);
        assertNull(mockDao.find("KZ_JAVA_WEB_DEV_00066"));
    }

    @Test
    public void shouldSuccessToFindProjectsByLocation() {
        int locationId = 103;
        List<Project> projectList = dao.findProjectsByLocation(locationId);
        assertFalse(projectList.isEmpty());
        assertEquals(locationId, projectList.get(0).getLocation().getLocation_id());
    }

    @Test
    public void shouldFailToFindProjectsIfLocationIdInvalid() {
        int locationId = -104;
        List<Project> projectList = dao.findProjectsByLocation(locationId);
        assertTrue(projectList.isEmpty());
    }

    @Test
    public void shouldSuccessToFindProjectsByDepartment() {
        String department = "KZ_DEP_DEV_APP_0003";
        List<Project> projectList = dao.findProjectsByDepartment(department);
        assertFalse(projectList.isEmpty());
        assertEquals(department, projectList.get(0).getLocation().getDepartment());
    }

    @Test
    public void shouldFailToFindProjectsIfDepartmentInvalid() {
        String department = "ABCDEFG";
        List<Project> projectList = dao.findProjectsByDepartment(department);
        assertTrue(projectList.isEmpty());
    }

    @Test
    public void shouldSuccessToFindAllProjects() {
        List<Project> projectList = dao.findAll();
        assertFalse(projectList.isEmpty());
    }

    @Test
    public void shouldFailToFindAllProjects() {
        List<Project> projects = new ArrayList<>();
        String FIND_ALL_SQL = "SELECT p.project_name, p.project_no, l.* FROM project p LEFT JOIN location l " +
                "ON p.location_id = l.location_id";
        when(mockTemplate.query(FIND_ALL_SQL, new ProjectRowMapper())).thenReturn(projects);
        assertTrue(mockDao.findAll().isEmpty());
    }

    @Test
    public void shouldSuccessToAddProjectIfProjectIsNew() {
        Project expectedProject = new Project("KZ_JAVA_WEB_DEV_00024", 104, new Location(113));
        assertTrue(dao.add(expectedProject));
        Project actualProject = dao.find("KZ_JAVA_WEB_DEV_00024");
        assertNotNull(actualProject);
        assertEquals(expectedProject.getProjectName(), actualProject.getProjectName());
    }

    @Test(expected = DuplicateKeyException .class)
    public void shouldThrowExceptionWhenAddProjectIfProjectExists() {
        Project testProject = new Project("KZ_JAVA_WEB_DEV_00004", 104, new Location(113));
        dao.add(testProject);
    }

    @Test
    public void shouldSuccessToUpdateProject() {
        Project oldProject = dao.find("KZ_JAVA_WEB_DEV_00004");
        Project newProject = new Project("KZ_JAVA_WEB_DEV_00004", 33, new Location(117));
        assertTrue(dao.update(newProject));
        Project actualProject = dao.find("KZ_JAVA_WEB_DEV_00004");
        assertNotNull(actualProject);
        assertNotEquals(oldProject.getProjectNo(), actualProject.getProjectNo());
        assertNotEquals(oldProject.getLocation().getLocation_id(), actualProject.getLocation().getLocation_id());
    }

    @Test
    public void shouldFailToUpdateProjectIfProjectNameNotExist() {
        Project testProject = new Project("ABCDEFG", 104, new Location(113));
        assertFalse(dao.update(testProject));
    }

    @Test
    public void shouldSuccessToUpdateProjectName() {
        assertTrue(dao.updateProjectName("KZ_JAVA_WEB_DEV_00004", "KZ_JAVA_WEB_DEV_00044"));
    }

    @Test
    public void shouldFailToUpdateProjectNameIfNameNotExist() {
        assertFalse(dao.updateProjectName("ABCDEFG", "KZ_JAVA_WEB_DEV_00044"));
    }

    @Test
    public void shouldSuccessToDeleteProjectWithLooseRelationship() {
        Project testProject = new Project("KZ_JAVA_WEB_DEV_00034", 111, new Location(113));
        assertTrue(dao.add(testProject));
        assertTrue(dao.delete("KZ_JAVA_WEB_DEV_00034"));
    }

    @Test(expected = DataIntegrityViolationException.class)
    public void shouldThrowExceptionWhenProjectCannotBeDeleted() {
        assertNotNull(dao.find("KZ_JAVA_WEB_DEV_00004"));
        dao.delete("KZ_JAVA_WEB_DEV_00004");
    }

    @Test
    public void shouldFailToDeleteProjectIfProjectNotExist() {
        assertFalse(dao.delete("ABCDEFG"));
    }
}