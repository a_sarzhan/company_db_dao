package dao.mapper;

import entity.Location;
import entity.Project;
import lombok.SneakyThrows;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.sql.ResultSet;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ProjectRowMapperTest {
    @Mock
    private ResultSet mockSet;

    @SneakyThrows
    @Test
    public void testProjectRowMapper() {
        populateMockSet();
        ProjectRowMapper mapper = new ProjectRowMapper();
        Project project = mapper.mapRow(mockSet, 1);
        Location location = project.getLocation();
        assertNotNull(project);
        assertNotNull(location);
        assertEquals("KZ_JAVA_WEB_DEV_00004", project.getProjectName());
        assertEquals(10, project.getProjectNo());
        assertEquals(114, location.getLocation_id());
        assertEquals("Kazakhstan", location.getCountry());
        assertNull(location.getStateProv());
        assertEquals("Almaty", location.getCity());
        assertEquals("Abay", location.getStreet());
        assertEquals(55, location.getApartmentNo());
        assertEquals("050001", location.getZipCode());
        assertEquals("KZ_DEP_DEV_APP_0001", location.getDepartment());
    }

    @SneakyThrows
    private void populateMockSet() {
        when(mockSet.getString("project_name")).thenReturn("KZ_JAVA_WEB_DEV_00004");
        when(mockSet.getInt("project_no")).thenReturn(10);
        when(mockSet.getInt("location_id")).thenReturn(114);
        when(mockSet.getString("country")).thenReturn("Kazakhstan");
        when(mockSet.getString("state_prov")).thenReturn(null);
        when(mockSet.getString("city")).thenReturn("Almaty");
        when(mockSet.getString("street")).thenReturn("Abay");
        when(mockSet.getInt("apartment_no")).thenReturn(55);
        when(mockSet.getString("zip_code")).thenReturn("050001");
        when(mockSet.getString("department")).thenReturn("KZ_DEP_DEV_APP_0001");
    }
}