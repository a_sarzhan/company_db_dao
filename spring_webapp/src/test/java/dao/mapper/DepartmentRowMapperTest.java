package dao.mapper;

import entity.Department;
import entity.Employee;
import lombok.SneakyThrows;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.ResultSet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DepartmentRowMapperTest {
    @Mock
    private ResultSet mockSet;

    @SneakyThrows
    @Test
    public void testMapRow() {
        populateMockSet();
        DepartmentRowMapper mapper = new DepartmentRowMapper();
        Department department = mapper.mapRow(mockSet, 1);
        Employee manager = department.getManager();
        assertNotNull(department);
        assertNotNull(manager);
        assertEquals("KZ_DEP_DEV_APP_0001", department.getDepartmentName());
        assertEquals(10008, manager.getEmployeeId());
        assertEquals("Amaliya", manager.getFirstName());
        assertEquals("Satbaldiyeva", manager.getLastName());
        assertEquals("female", manager.getGender());
        assertEquals(new Date(8000), manager.getBirthDate());
        assertEquals(new BigDecimal("400000.00"), manager.getSalary());
        assertEquals(10005, manager.getSupervisorId());
        assertEquals("KZ_DEP_DEV_APP_0001", manager.getDepartment());
    }

    @SneakyThrows
    private void populateMockSet() {
        when(mockSet.getString("department_name")).thenReturn("KZ_DEP_DEV_APP_0001");
        when(mockSet.getInt("employee_id")).thenReturn(10008);
        when(mockSet.getString("first_name")).thenReturn("Amaliya");
        when(mockSet.getString("last_name")).thenReturn("Satbaldiyeva");
        when(mockSet.getString("gender")).thenReturn("female");
        when(mockSet.getDate("birth_date")).thenReturn(new Date(8000));
        when(mockSet.getBigDecimal("salary")).thenReturn(new BigDecimal("400000.00"));
        when(mockSet.getInt("supervisor_id")).thenReturn(10005);
        when(mockSet.getString("department")).thenReturn("KZ_DEP_DEV_APP_0001");
    }
}