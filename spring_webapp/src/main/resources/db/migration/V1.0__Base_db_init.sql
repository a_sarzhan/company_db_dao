CREATE TABLE public.department
(
    department_name character varying(50) NOT NULL,
    manager_id integer,
    CONSTRAINT department_pkey PRIMARY KEY (department_name)
);
ALTER TABLE public.department OWNER TO postgres;

CREATE TABLE public.employee (
    employee_id integer NOT NULL,
    first_name character varying(15) NOT NULL,
    last_name character varying(30) NOT NULL,
    birth_date date NOT NULL,
    gender character varying(10) NOT NULL,
    salary numeric,
    supervisor_id integer NOT NULL,
    department character varying(50) NOT NULL,
    CONSTRAINT employee_pkey PRIMARY KEY (employee_id),
    CONSTRAINT supervisor_validation CHECK (supervisor_id <> employee_id)
);
ALTER TABLE public.employee OWNER TO postgres;

CREATE TABLE public.location (
    location_id integer NOT NULL,
    country character varying(30) NOT NULL,
    state_prov character varying(30),
    city character varying(30),
    street character varying(30),
    apartment_no integer,
    zip_code character varying(10),
    department character varying(50),
    CONSTRAINT location_pkey PRIMARY KEY (location_id)
);
ALTER TABLE public.location OWNER TO postgres;

CREATE TABLE public.project (
    project_name character varying(50) NOT NULL,
    project_no integer NOT NULL,
    location_id integer NOT NULL,
    CONSTRAINT project_pkey PRIMARY KEY (project_name)
);
ALTER TABLE public.project OWNER TO postgres;

CREATE TABLE public.project_workload (
    project_load_id integer NOT NULL,
    project_name character varying(30) NOT NULL,
    employee_id integer NOT NULL,
    work_hours_week double precision,
    CONSTRAINT project_workload_pkey PRIMARY KEY (project_load_id)
);
ALTER TABLE public.project_workload OWNER TO postgres;

CREATE TABLE public.employee_address (
    address_id integer NOT NULL,
    country character varying(30) NOT NULL,
    state character varying(30),
    city character varying(30) NOT NULL,
    street character varying(30) NOT NULL,
    apartment_no integer,
    flat_no integer,
    employee_id integer,
    CONSTRAINT employee_address_pkey PRIMARY KEY (address_id)
);
ALTER TABLE public.employee_address OWNER TO postgres;

CREATE TABLE public.logging (
    insert_date timestamp without time zone,
    ref_table character varying(30),
    description character varying
);
ALTER TABLE public.logging OWNER TO postgres;

ALTER TABLE ONLY public.department
    ADD CONSTRAINT department_manager_id_fkey FOREIGN KEY (manager_id) REFERENCES public.employee(employee_id) ON UPDATE CASCADE;

ALTER TABLE ONLY public.employee
    ADD CONSTRAINT employee_department_fkey FOREIGN KEY (department) REFERENCES public.department(department_name) ON UPDATE CASCADE;

ALTER TABLE ONLY public.employee_address
    ADD CONSTRAINT employee_id_fkey FOREIGN KEY (employee_id) REFERENCES public.employee(employee_id) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE ONLY public.employee
    ADD CONSTRAINT employee_supervisor_id_fkey FOREIGN KEY (supervisor_id) REFERENCES public.employee(employee_id) ON UPDATE CASCADE;

ALTER TABLE ONLY public.location
    ADD CONSTRAINT location_department_fkey FOREIGN KEY (department) REFERENCES public.department(department_name) ON UPDATE CASCADE;

ALTER TABLE ONLY public.project
    ADD CONSTRAINT project_location_id_fkey FOREIGN KEY (location_id) REFERENCES public.location(location_id) ON UPDATE CASCADE;

ALTER TABLE ONLY public.project_workload
    ADD CONSTRAINT project_workload_employee_id_fkey FOREIGN KEY (employee_id) REFERENCES public.employee(employee_id) ON UPDATE CASCADE;

ALTER TABLE ONLY public.project_workload
    ADD CONSTRAINT project_workload_project_name_fkey FOREIGN KEY (project_name) REFERENCES public.project(project_name) ON UPDATE CASCADE;


------------------- BASE FUNCTIONS AND TRIGGERS -----------------
CREATE EXTENSION IF NOT EXISTS hstore;
CREATE FUNCTION public.audit_inserted_tables() RETURNS trigger
    LANGUAGE plpgsql SECURITY DEFINER
    SET search_path TO 'public'
    AS $$
DECLARE
	_key text;
	_value text;
	description text;
BEGIN
	IF (TG_OP = 'INSERT') THEN
		FOR _key, _value IN
			SELECT n.key, n.value
			FROM each(hstore(NEW)) n
			WHERE n.value IS NOT NULL
		LOOP
			description := CONCAT(description, _key, '-', _value, ';');
		END LOOP;

		INSERT INTO public.logging
		VALUES (now(), TG_TABLE_NAME, description);
		RETURN NEW;
	END IF;

END;
$$;

ALTER FUNCTION public.audit_inserted_tables() OWNER TO postgres;

CREATE FUNCTION public.get_projects(dep_name character varying, location_id integer, separator character) RETURNS text
    LANGUAGE plpgsql
    AS $_$
declare
    var_r record;
	projects text;
begin
	for var_r in(
			select p.project_name
			from project p
			inner join location l
			on p.location_id = l.location_id
			where l.department = $1
			and p.location_id = $2
    ) loop
			projects := CONCAT(projects, var_r.project_name, $3);
	end loop;
	return projects;
end; $_$;

ALTER FUNCTION public.get_projects(dep_name character varying, location_id integer, separator character) OWNER TO postgres;

CREATE TRIGGER t_if_inserted_department AFTER INSERT ON public.department FOR EACH ROW EXECUTE PROCEDURE public.audit_inserted_tables();

CREATE TRIGGER t_if_inserted_emp_address AFTER INSERT ON public.employee_address FOR EACH ROW EXECUTE PROCEDURE public.audit_inserted_tables();

CREATE TRIGGER t_if_inserted_employee AFTER INSERT ON public.employee FOR EACH ROW EXECUTE PROCEDURE public.audit_inserted_tables();

CREATE TRIGGER t_if_inserted_location AFTER INSERT ON public.location FOR EACH ROW EXECUTE PROCEDURE public.audit_inserted_tables();

CREATE TRIGGER t_if_inserted_project AFTER INSERT ON public.project FOR EACH ROW EXECUTE PROCEDURE public.audit_inserted_tables();

CREATE TRIGGER t_if_inserted_project_load AFTER INSERT ON public.project_workload FOR EACH ROW EXECUTE PROCEDURE public.audit_inserted_tables();