CREATE INDEX employee_index ON employee (employee_id);
CREATE INDEX project_workload_index ON project_workload (project_name, employee_id);