CREATE OR REPLACE VIEW public.department_state_view
 AS
 WITH cte_emp AS (
         SELECT q1.department_name,
            count(e.employee_id) AS employee_amount,
            q1.manager_name
           FROM ( SELECT d.department_name,
                    concat(e_1.first_name, ' ', e_1.last_name) AS manager_name
                   FROM department d
                     LEFT JOIN employee e_1 ON d.manager_id = e_1.employee_id) q1
             LEFT JOIN employee e ON q1.department_name::text = e.department::text
          GROUP BY q1.department_name, q1.manager_name
        ), cte_ratio AS (
         SELECT employee.department,
            sum(
                CASE
                    WHEN employee.gender::text = 'male'::text THEN 1
                    ELSE 0
                END)::double precision / count(*)::double precision AS male_ratio,
            sum(
                CASE
                    WHEN employee.gender::text = 'female'::text THEN 1
                    ELSE 0
                END)::double precision / count(*)::double precision AS female_ratio
           FROM employee
          GROUP BY employee.department
        ), cte_dep AS (
         SELECT l.department,
            count(p.project_name) AS project_amount
           FROM location l
             JOIN project p ON l.location_id = p.location_id
          GROUP BY l.department
        ), cte_project_hours AS (
         SELECT l.department,
            pr.project_name,
            sum(pw.work_hours_week) AS total_hours
           FROM location l
             JOIN project pr ON l.location_id = pr.location_id
             LEFT JOIN project_workload pw ON pr.project_name::text = pw.project_name::text
          GROUP BY l.department, pr.project_name
        ), cte_max AS (
         SELECT c1.department,
            c1.project_name AS max_hours_project,
            c1.total_hours AS max_hours
           FROM cte_project_hours c1
          WHERE c1.total_hours = (( SELECT max(c2.total_hours) AS max
                   FROM cte_project_hours c2
                  WHERE c2.department::text = c1.department::text
                  GROUP BY c2.department))
        ), cte_min AS (
         SELECT c1.department,
            c1.project_name AS min_hours_project,
            c1.total_hours AS min_hours
           FROM cte_project_hours c1
          WHERE c1.total_hours = (( SELECT min(c2.total_hours) AS min
                   FROM cte_project_hours c2
                  WHERE c2.department::text = c1.department::text
                  GROUP BY c2.department))
        )
 SELECT ce.department_name,
    ce.employee_amount,
    ce.manager_name,
    cr.male_ratio,
    cr.female_ratio,
    cd.project_amount,
    cm.max_hours_project,
    cx.min_hours_project
   FROM cte_emp ce
     LEFT JOIN cte_ratio cr ON ce.department_name::text = cr.department::text
     LEFT JOIN cte_dep cd ON ce.department_name::text = cd.department::text
     LEFT JOIN cte_max cm ON ce.department_name::text = cm.department::text
     LEFT JOIN cte_min cx ON ce.department_name::text = cx.department::text
  ORDER BY ce.department_name;

ALTER TABLE public.department_state_view
    OWNER TO postgres;