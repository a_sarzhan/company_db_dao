DELETE FROM project_workload;

ALTER TABLE project_workload
DROP CONSTRAINT project_workload_pkey;

ALTER TABLE project_workload
DROP COLUMN IF EXISTS project_load_id;

ALTER TABLE project_workload
ADD CONSTRAINT project_workload_composite_pkey PRIMARY KEY(project_name, employee_id);