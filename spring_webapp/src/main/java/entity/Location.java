package entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Location {
    private int location_id;
    private String country;
    private String stateProv;
    private String city;
    private String street;
    private int apartmentNo;
    private String zipCode;
    private String department;

    public Location(int location_id) {
        this.location_id = location_id;
    }
}
