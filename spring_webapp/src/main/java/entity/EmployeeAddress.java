package entity;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class EmployeeAddress {
    private int addressId;
    private String country;
    private String state;
    private String city;
    private String street;
    private int apartmentNo;
    private int flatNo;
    private int employeeId;

    public EmployeeAddress(int addressId) {
        this.addressId = addressId;
    }
}
