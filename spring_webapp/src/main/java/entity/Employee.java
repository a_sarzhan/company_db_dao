package entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Employee {
    private int employeeId;
    private String firstName;
    private String lastName;
    private Date birthDate;
    private String gender;
    private BigDecimal salary;
    private int supervisorId;
    private String department;

    public Employee(int employeeId) {
        this.employeeId = employeeId;
    }
}
