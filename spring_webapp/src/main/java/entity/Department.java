package entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Department {
    private String departmentName;
    private Employee manager;
    private List<Location> locations;

    public Department(String departmentName) {
        this.departmentName = departmentName;
    }
}
