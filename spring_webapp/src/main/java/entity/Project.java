package entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Project {
    private String projectName;
    private int projectNo;
    private Location location;

    public Project(String projectName) {
        this.projectName = projectName;
    }
}
