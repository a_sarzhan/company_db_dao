package dao;

public interface GenericDao<T, ID> {
    public T find(ID id);
    public boolean add(T object);
    public boolean update(T object);
    public boolean delete(ID id);
}
