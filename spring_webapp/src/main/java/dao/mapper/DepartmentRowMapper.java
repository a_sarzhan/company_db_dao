package dao.mapper;

import entity.Department;
import entity.Employee;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DepartmentRowMapper implements RowMapper<Department> {
    @Override
    public Department mapRow(ResultSet resultSet, int i) throws SQLException {
        Department department = new Department();
        Employee manager = new Employee();
        department.setDepartmentName(resultSet.getString("department_name"));
        manager.setEmployeeId(resultSet.getInt("employee_id"));
        manager.setFirstName(resultSet.getString("first_name"));
        manager.setLastName(resultSet.getString("last_name"));
        manager.setGender(resultSet.getString("gender"));
        manager.setBirthDate(resultSet.getDate("birth_date"));
        manager.setSalary(resultSet.getBigDecimal("salary"));
        manager.setSupervisorId(resultSet.getInt("supervisor_id"));
        manager.setDepartment(resultSet.getString("department"));
        department.setManager(manager);
        return department;
    }
}
