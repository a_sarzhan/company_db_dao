package dao.mapper;

import entity.Location;
import entity.Project;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ProjectRowMapper implements RowMapper<Project> {
    @Override
    public Project mapRow(ResultSet resultSet, int i) throws SQLException {
        Project project = new Project();
        Location location = new Location();
        project.setProjectName(resultSet.getString("project_name"));
        project.setProjectNo(resultSet.getInt("project_no"));
        location.setLocation_id(resultSet.getInt("location_id"));
        location.setCountry(resultSet.getString("country"));
        location.setStateProv(resultSet.getString("state_prov"));
        location.setCity(resultSet.getString("city"));
        location.setStreet(resultSet.getString("street"));
        location.setApartmentNo(resultSet.getInt("apartment_no"));
        location.setZipCode(resultSet.getString("zip_code"));
        location.setDepartment(resultSet.getString("department"));
        project.setLocation(location);
        return project;
    }
}
