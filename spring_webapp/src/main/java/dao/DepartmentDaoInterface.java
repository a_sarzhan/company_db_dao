package dao;

import entity.Department;

import java.util.List;

public interface DepartmentDaoInterface {
    List<Department> findAll();
    Department findDepartmentByManager(Integer managerId);
    boolean updateDepartmentName(String oldDepartmentName, Department newDepartment);
}
