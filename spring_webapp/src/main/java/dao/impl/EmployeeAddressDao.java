package dao.impl;

import dao.GenericDao;
import entity.EmployeeAddress;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;

@Slf4j
@Component
public class EmployeeAddressDao implements GenericDao<EmployeeAddress, Integer> {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    private static final String FIND_SQL = "SELECT * FROM employee_address WHERE address_id = ?";
    private static final String FIND_BY_EMPLOYEE_SQL = "SELECT * FROM employee_address WHERE employee_id = ?";
    private static final String ADD_SQL = "INSERT INTO employee_address VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
    private static final String UPDATE_SQL = "UPDATE employee_address SET country = ?, state = ?, city = ?, street = ?, " +
            "apartment_no = ?, flat_no = ?, employee_id = ? WHERE address_id = ?";
    private static final String DELETE_SQL = "DELETE FROM employee_address WHERE address_id = ?";

    @Override
    public EmployeeAddress find(Integer addressId) {
        EmployeeAddress address = null;
        try {
            address = Objects.requireNonNull(jdbcTemplate).queryForObject(FIND_SQL, new Object[]{addressId},
                    BeanPropertyRowMapper.newInstance(EmployeeAddress.class));
        } catch (EmptyResultDataAccessException ex) {
            log.error("No employee address found by ID: " + addressId + ". Error: " + ex.getLocalizedMessage());
        }
        return address;
    }

    public List<EmployeeAddress> findAddressByEmployeeId(Integer employeeId) {
        return Objects.requireNonNull(jdbcTemplate).query(FIND_BY_EMPLOYEE_SQL, new Object[]{employeeId},
                BeanPropertyRowMapper.newInstance(EmployeeAddress.class));
    }

    @Override
    public boolean add(EmployeeAddress address) {
        Object[] args = new Object[]{address.getAddressId(), address.getCountry(), address.getState(), address.getCity(),
                address.getStreet(), address.getApartmentNo(), address.getFlatNo(), address.getEmployeeId()};
        return Objects.requireNonNull(jdbcTemplate).update(ADD_SQL, args) == 1;
    }

    @Override
    public boolean update(EmployeeAddress address) {
        Object[] args = new Object[]{address.getCountry(), address.getState(), address.getCity(), address.getStreet(),
                address.getApartmentNo(), address.getFlatNo(), address.getEmployeeId(), address.getAddressId()};
        return Objects.requireNonNull(jdbcTemplate).update(UPDATE_SQL, args) == 1;
    }

    @Override
    public boolean delete(Integer addressId) {
        return Objects.requireNonNull(jdbcTemplate).update(DELETE_SQL, addressId) == 1;
    }
}
