package dao.impl;

import entity.ProjectWorkload;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
public class ProjectWorkloadDao {
    @Autowired
    private NamedParameterJdbcTemplate parameterTemplate;
    private static final String FIND_SQL = "SELECT * FROM project_workload WHERE project_name=:projectName " +
            "AND employee_id=:employeeId";
    private static final String FIND_BY_PROJECT_SQL = "SELECT * FROM project_workload WHERE project_name=:projectName";
    private static final String FIND_BY_EMPLOYEE_SQL = "SELECT * FROM project_workload WHERE employee_id=:employeeId";
    private static final String INSERT_SQL = "INSERT INTO project_workload VALUES (:projectName, :employeeId, " +
            ":workHoursWeek)";
    private static final String UPDATE_SQL = "UPDATE project_workload SET work_hours_week=:workHoursWeek " +
            "WHERE project_name=:projectName AND employee_id=:employeeId";
    private static final String DELETE_SINGLE_SQL = "DELETE FROM project_workload WHERE project_name=:projectName " +
            "AND employee_id=:employeeId";
    private static final String DELETE_BY_PROJECT_SQL = "DELETE FROM project_workload WHERE project_name=:projectName";

    public ProjectWorkload find(String projectName, Integer employeeId) {
        ProjectWorkload projectLoad = null;
        try {
            MapSqlParameterSource params = new MapSqlParameterSource("projectName", projectName)
                    .addValue("employeeId", employeeId);
            projectLoad = parameterTemplate.queryForObject(FIND_SQL, params, BeanPropertyRowMapper.newInstance(ProjectWorkload.class));
        } catch (EmptyResultDataAccessException ex) {
            log.error("ProjectWorkload not found! Project Name: " + projectName + ", Employee ID: " + employeeId + ". " +
                    "Error: " + ex.getLocalizedMessage());
        }
        return projectLoad;
    }

    public List<ProjectWorkload> findProjectWorkloadByProject(String projectName) {
        MapSqlParameterSource params = new MapSqlParameterSource("projectName", projectName);
        return parameterTemplate.query(FIND_BY_PROJECT_SQL, params, BeanPropertyRowMapper.newInstance(ProjectWorkload.class));
    }

    public List<ProjectWorkload> findProjectWorkloadByEmployee(Integer employeeId) {
        MapSqlParameterSource params = new MapSqlParameterSource("employeeId", employeeId);
        return parameterTemplate.query(FIND_BY_EMPLOYEE_SQL, params, BeanPropertyRowMapper.newInstance(ProjectWorkload.class));
    }

    public boolean add(ProjectWorkload projectLoad) {
        MapSqlParameterSource params = getSqlParameters(projectLoad);
        return parameterTemplate.update(INSERT_SQL, params) == 1;
    }

    public boolean update(ProjectWorkload projectLoad) {
        MapSqlParameterSource params = getSqlParameters(projectLoad);
        return parameterTemplate.update(UPDATE_SQL, params) == 1;
    }

    public void deleteSingleProjectWorkload(String projectName, Integer employeeId) {
        MapSqlParameterSource params = new MapSqlParameterSource("projectName", projectName)
                .addValue("employeeId", employeeId);
        parameterTemplate.update(DELETE_SINGLE_SQL, params);
    }

    public void deleteWorkLoadByProject(String projectName) {
        MapSqlParameterSource params = new MapSqlParameterSource("projectName", projectName);
        parameterTemplate.update(DELETE_BY_PROJECT_SQL, params);
    }

    private MapSqlParameterSource getSqlParameters(ProjectWorkload projectLoad) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("projectName", projectLoad.getProjectName())
                .addValue("employeeId", projectLoad.getEmployeeId())
                .addValue("workHoursWeek", projectLoad.getWorkHoursWeek());
        return params;
    }
}
