package dao.impl;

import dao.GenericDao;
import dao.ProjectDaoInterface;
import dao.mapper.ProjectRowMapper;
import entity.Project;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
public class ProjectDao implements GenericDao<Project, String>, ProjectDaoInterface {
    @Autowired
    private NamedParameterJdbcTemplate parameterTemplate;
    private static final String FIND_SQL = "SELECT p.project_name, p.project_no, l.* FROM project p LEFT JOIN location l " +
            "ON p.location_id = l.location_id WHERE p.project_name=:projectName";
    private static final String FIND_BY_LOCATION_SQL = "SELECT p.project_name, p.project_no, l.* FROM project p LEFT JOIN location l " +
            "ON p.location_id = l.location_id WHERE p.location_id=:locationId";
    private static final String FIND_ALL_SQL = "SELECT p.project_name, p.project_no, l.* FROM project p LEFT JOIN location l " +
            "ON p.location_id = l.location_id";
    private static final String FIND_BY_DEPARTMENT_SQL = "SELECT p.project_name, p.project_no, l.* FROM project p LEFT JOIN location l " +
            "ON p.location_id = l.location_id WHERE l.department=:departmentName";
    private static final String INSERT_SQL = "INSERT INTO project VALUES (:projectName, :projectNo, :locationId)";
    private static final String UPDATE_SQL = "UPDATE project SET project_no=:projectNo, location_id=:locationId " +
            "WHERE project_name=:projectName";
    private static final String UPDATE_PROJECT_NAME_SQL = "UPDATE project SET project_name=:newName WHERE project_name=:oldName";
    private static final String DELETE_SQL = "DELETE FROM project WHERE project_name=:projectName";

    @Override
    public Project find(String projectName) {
        Project project = null;
        MapSqlParameterSource params = new MapSqlParameterSource("projectName", projectName);
        try {
            project = parameterTemplate.queryForObject(FIND_SQL, params, new ProjectRowMapper());
        } catch (EmptyResultDataAccessException ex) {
            log.error("Project not found. Project Name: " + projectName + ". Error: " + ex.getLocalizedMessage());
        }
        return project;
    }

    @Override
    public List<Project> findProjectsByLocation(Integer locationId) {
        MapSqlParameterSource params = new MapSqlParameterSource("locationId", locationId);
        return parameterTemplate.query(FIND_BY_LOCATION_SQL, params, new ProjectRowMapper());
    }

    @Override
    public List<Project> findProjectsByDepartment(String departmentName) {
        MapSqlParameterSource params = new MapSqlParameterSource("departmentName", departmentName);
        return parameterTemplate.query(FIND_BY_DEPARTMENT_SQL, params, new ProjectRowMapper());
    }

    @Override
    public List<Project> findAll() {
        return parameterTemplate.query(FIND_ALL_SQL, new ProjectRowMapper());
    }

    @Override
    public boolean add(Project project) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("projectName", project.getProjectName())
                .addValue("projectNo", project.getProjectNo())
                .addValue("locationId", project.getLocation().getLocation_id());
        return parameterTemplate.update(INSERT_SQL, params) == 1;
    }

    @Override
    public boolean update(Project project) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("projectName", project.getProjectName())
                .addValue("projectNo", project.getProjectNo())
                .addValue("locationId", project.getLocation().getLocation_id());
        return parameterTemplate.update(UPDATE_SQL, params) == 1;
    }

    @Override
    public boolean updateProjectName(String oldProjectName, String newProjectName) {
        MapSqlParameterSource params = new MapSqlParameterSource("newName", newProjectName)
                .addValue("oldName", oldProjectName);
        return parameterTemplate.update(UPDATE_PROJECT_NAME_SQL, params) == 1;
    }

    @Override
    public boolean delete(String projectName) {
        return parameterTemplate.update(DELETE_SQL, new MapSqlParameterSource("projectName", projectName)) == 1;
    }
}
