package dao.impl;

import dao.DepartmentDaoInterface;
import dao.GenericDao;
import dao.mapper.DepartmentRowMapper;
import entity.Department;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
public class DepartmentDao implements GenericDao<Department, String>, DepartmentDaoInterface {
    @Autowired
    private NamedParameterJdbcTemplate parameterTemplate;
    private static final String FIND_SQL = "SELECT d.department_name, e.* FROM department d LEFT JOIN employee e " +
            "ON d.manager_id = e.employee_id WHERE d.department_name=:department";
    private static final String FIND_ALL_SQL = "SELECT d.department_name, e.* FROM department d LEFT JOIN employee e " +
            "ON d.manager_id = e.employee_id";
    private static final String FIND_DEPARTMENT_SQL = "SELECT d.department_name, e.* FROM department d LEFT JOIN employee e " +
            "ON d.manager_id = e.employee_id WHERE d.manager_id=:managerId LIMIT 1";
    private static final String INSERT_SQL = "INSERT INTO department VALUES (:department, :managerId)";
    private static final String UPDATE_DEPARTMENT_SQL = "UPDATE department SET department_name=:newDepartment, manager_id=:managerId WHERE department_name=:department";
    private static final String UPDATE_SQL = "UPDATE department SET manager_id=:managerId WHERE department_name=:department";
    private static final String DELETE_SQL = "DELETE FROM department WHERE department_name=:department";

    @Override
    public Department find(String departmentName) {
        MapSqlParameterSource params = new MapSqlParameterSource("department", departmentName);
        Department department = null;
        try {
            department = parameterTemplate.queryForObject(FIND_SQL, params, new DepartmentRowMapper());
        } catch (EmptyResultDataAccessException ex) {
            log.error("Department not found. Department Name: " + departmentName + ". Error: " + ex.getLocalizedMessage());
        }
        return department;
    }

    @Override
    public List<Department> findAll() {
        return parameterTemplate.query(FIND_ALL_SQL, new DepartmentRowMapper());
    }

    @Override
    public Department findDepartmentByManager(Integer managerId) {
        MapSqlParameterSource params = new MapSqlParameterSource("managerId", managerId);
        Department department = null;
        try {
            department = parameterTemplate.queryForObject(FIND_DEPARTMENT_SQL, params, new DepartmentRowMapper());
        } catch (EmptyResultDataAccessException ex) {
            log.error("Department not found. Manager ID: " + managerId + ". Error: " + ex.getLocalizedMessage());
        }
        return department;
    }

    @Override
    public boolean add(Department department) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("department", department.getDepartmentName())
                .addValue("managerId", department.getManager().getEmployeeId());
        return parameterTemplate.update(INSERT_SQL, params) == 1;
    }

    @Override
    public boolean update(Department department) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("managerId", department.getManager().getEmployeeId())
                .addValue("department", department.getDepartmentName());
        return parameterTemplate.update(UPDATE_SQL, params) == 1;
    }

    @Override
    public boolean updateDepartmentName(String oldDepartmentName, Department newDepartment) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("newDepartment", newDepartment.getDepartmentName())
                .addValue("managerId", newDepartment.getManager().getEmployeeId())
                .addValue("department", oldDepartmentName);
        return parameterTemplate.update(UPDATE_DEPARTMENT_SQL, params) == 1;
    }

    @Override
    public boolean delete(String departmentName) {
        MapSqlParameterSource params = new MapSqlParameterSource("department", departmentName);
        return parameterTemplate.update(DELETE_SQL, params) == 1;
    }
}
