package dao.impl;

import dao.EmployeeDaoInterface;
import dao.GenericDao;
import entity.Employee;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
public class EmployeeDao implements GenericDao<Employee, Integer>, EmployeeDaoInterface {
    @Autowired
    private NamedParameterJdbcTemplate namedTemplate;
    private static final String FIND_EMPLOYEE_SQL = "SELECT * FROM employee where employee_id=:employeeId";
    private static final String FIND_BY_DEPARTMENT_SQL = "SELECT * FROM employee where department=:department";
    private static final String FIND_BY_SUPERVISOR_SQL = "SELECT * FROM employee where supervisor_id=:supervisor";
    private static final String FIND_BY_NAME_SQL = "SELECT * FROM employee where first_name=:firstName and last_name=:lastName";
    private static final String FIND_ALL_SQL = "SELECT * FROM employee";
    private static final String INSERT_SQL = "INSERT INTO employee VALUES (:employeeId, :name, :lastName, :birthDate, :gender, " +
            ":salary, :supervisor, :department)";
    private static final String UPDATE_SQL = "UPDATE employee SET first_name=:name, last_name=:lastName, birth_date=:birthDate," +
            "gender=:gender, salary=:salary, supervisor_id=:supervisor, department=:department WHERE employee_id=:employeeId";
    private static final String UPDATE_SUPERVISOR_SQL = "UPDATE employee SET supervisor_id=:supervisor WHERE employee_id=:employeeId";
    private static final String DELETE_SQL = "DELETE FROM employee WHERE employee_id=:employeeId";

    @Override
    public Employee find(Integer employeeId) {
        Employee employee = null;
        MapSqlParameterSource params = new MapSqlParameterSource("employeeId", employeeId);
        try {
            employee = namedTemplate.queryForObject(FIND_EMPLOYEE_SQL, params, BeanPropertyRowMapper.newInstance(Employee.class));
        } catch (EmptyResultDataAccessException ex) {
            log.error("Employee not found! Given Id: " + employeeId + ". Error: " + ex.getLocalizedMessage());
        }
        return employee;
    }

    @Override
    public List<Employee> findEmployeesByDepartment(String departmentName) {
        MapSqlParameterSource params = new MapSqlParameterSource("department", departmentName);
        return namedTemplate.query(FIND_BY_DEPARTMENT_SQL, params, BeanPropertyRowMapper.newInstance(Employee.class));
    }

    @Override
    public List<Employee> findEmployeesBySupervisor(Integer supervisorId) {
        MapSqlParameterSource params = new MapSqlParameterSource("supervisor", supervisorId);
        return namedTemplate.query(FIND_BY_SUPERVISOR_SQL, params, BeanPropertyRowMapper.newInstance(Employee.class));
    }

    @Override
    public List<Employee> findEmployeesByName(String firstName, String lastName) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("firstName", firstName)
                .addValue("lastName", lastName);
        return namedTemplate.query(FIND_BY_NAME_SQL, params, BeanPropertyRowMapper.newInstance(Employee.class));
    }

    @Override
    public List<Employee> findAll() {
        return namedTemplate.query(FIND_ALL_SQL, BeanPropertyRowMapper.newInstance(Employee.class));
    }

    @Override
    public boolean add(Employee employee) {
        MapSqlParameterSource params = getSqlSourceParams(employee);
        return namedTemplate.update(INSERT_SQL, params) == 1;                   //expected = NullPointerException.class
    }

    @Override
    public boolean update(Employee employee) {
        MapSqlParameterSource params = getSqlSourceParams(employee);
        return namedTemplate.update(UPDATE_SQL, params) == 1;
    }

    @Override
    public boolean updateSupervisor(Integer employeeId, Integer supervisorId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("supervisor", supervisorId)
                .addValue("employeeId", employeeId);
        return namedTemplate.update(UPDATE_SUPERVISOR_SQL, params) == 1;
    }

    @Override
    public boolean delete(Integer employeeId) {
        MapSqlParameterSource params = new MapSqlParameterSource("employeeId", employeeId);
        return namedTemplate.update(DELETE_SQL, params) == 1;
    }

    private MapSqlParameterSource getSqlSourceParams(Employee employee) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("employeeId", employee.getEmployeeId())
                .addValue("name", employee.getFirstName())
                .addValue("lastName", employee.getLastName())
                .addValue("birthDate", employee.getBirthDate())
                .addValue("gender", employee.getGender())
                .addValue("salary", employee.getSalary())
                .addValue("supervisor", employee.getSupervisorId())
                .addValue("department", employee.getDepartment());
        return params;
    }
}
