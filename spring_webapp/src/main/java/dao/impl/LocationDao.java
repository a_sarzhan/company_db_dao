package dao.impl;

import dao.GenericDao;
import entity.Location;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
public class LocationDao implements GenericDao<Location, Integer> {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    private static final String FIND_SQL = "SELECT * FROM location WHERE location_id = ?";
    private static final String FIND_ALL_SQL = "SELECT * FROM location";
    private static final String FIND_BY_DEPARTMENT_SQL = "SELECT * FROM location WHERE department = ?";
    private static final String INSERT_SQL = "INSERT INTO location VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
    private static final String UPDATE_SQL = "UPDATE location SET country = ?, state_prov = ?, city = ?, " +
            "street = ?, apartment_no = ?, zip_code = ?, department = ? WHERE location_id = ?";
    private static final String DELETE_SQL = "DELETE FROM location WHERE location_id = ?";

    @Override
    public Location find(Integer locationId) {
        Location location = null;
        try {
            location = jdbcTemplate.queryForObject(FIND_SQL, new Object[]{locationId}, BeanPropertyRowMapper.newInstance(Location.class));
        } catch (EmptyResultDataAccessException ex) {
            log.error("No employee address found by ID: " + locationId + ". Error: " + ex.getLocalizedMessage());
        }
        return location;
    }

    public List<Location> findAllLocations() {
        return jdbcTemplate.query(FIND_ALL_SQL, BeanPropertyRowMapper.newInstance(Location.class));
    }

    public List<Location> findLocationsByDepartment(String departmentName) {
        return jdbcTemplate.query(FIND_BY_DEPARTMENT_SQL, new Object[]{departmentName},
                BeanPropertyRowMapper.newInstance(Location.class));
    }

    @Override
    public boolean add(Location location) {
        Object[] args = new Object[]{location.getLocation_id(), location.getCountry(),
                location.getStateProv(), location.getCity(), location.getStreet(),
                location.getApartmentNo(), location.getZipCode(), location.getDepartment()};
        return jdbcTemplate.update(INSERT_SQL, args) == 1;
    }

    @Override
    public boolean update(Location location) {
        Object[] args = new Object[]{location.getCountry(), location.getStateProv(), location.getCity(), location.getStreet(),
                location.getApartmentNo(), location.getZipCode(), location.getDepartment(), location.getLocation_id()};
        return jdbcTemplate.update(UPDATE_SQL, args) == 1;
    }

    @Override
    public boolean delete(Integer locationId) {
        return jdbcTemplate.update(DELETE_SQL, locationId) == 1;
    }

}
