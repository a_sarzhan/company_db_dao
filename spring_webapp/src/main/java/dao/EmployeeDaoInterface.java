package dao;

import entity.Employee;

import java.util.List;

public interface EmployeeDaoInterface {
    List<Employee> findAll();
    List<Employee> findEmployeesByDepartment(String departmentName);
    List<Employee> findEmployeesBySupervisor(Integer supervisorId);
    List<Employee> findEmployeesByName(String firstName, String lastName);
    boolean updateSupervisor(Integer employeeId, Integer supervisorId);
}
