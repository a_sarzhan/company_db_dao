package dao;

import entity.Project;

import java.util.List;

public interface ProjectDaoInterface {
    List<Project> findProjectsByLocation(Integer locationId);
    List<Project> findProjectsByDepartment(String departmentName);
    List<Project> findAll();
    boolean updateProjectName(String oldProjectName, String newProjectName);
}
