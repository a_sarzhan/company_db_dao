import config.JavaConfig;
import dao.impl.ProjectWorkloadDao;
import entity.ProjectWorkload;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.text.ParseException;
import java.util.List;

public class App {

    public static void main(String[] args) throws ParseException {
        ApplicationContext context = new AnnotationConfigApplicationContext(JavaConfig.class);
        ProjectWorkloadDao dao = context.getBean("projectWorkloadDao", ProjectWorkloadDao.class);

        //System.out.println(dao.find(5048));
        ProjectWorkload load = new ProjectWorkload("KZ_JAVA_MOB_DEV_00003", 10005, 9.99);
        //dao.deleteSingleProjectWorkload("KZ_JAVA_MOB_DEV_00003", 10005);

        List<ProjectWorkload> loads = dao.findProjectWorkloadByProject("KZ_JAVA_MOB_DEV_00003");
        for (ProjectWorkload pw : loads) {
            System.out.println(pw);
        }
    }
}
